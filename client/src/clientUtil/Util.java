package clientUtil;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

import static application.StartApplication.memoryLogger;
import static gameInterfaces.dataClasses.Card.HEIGHT;
import static gameInterfaces.dataClasses.Card.WIDTH;
import static java.awt.RenderingHints.KEY_INTERPOLATION;
import static java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR;

public class Util {

    private static String TOKEN_FILE = "token";

    public static void saveToken(String token) throws FileNotFoundException {

        File tokenFile = new File(TOKEN_FILE);
        try {
            tokenFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        PrintWriter writer = new PrintWriter(tokenFile);
        writer.write(token);
        writer.close();
    }

    public static String readToken() throws IOException {

        BufferedReader reader = new BufferedReader(new FileReader(new File(TOKEN_FILE)));
        return reader.readLine();
    }
}
