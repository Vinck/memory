package controllers;

import brokerEntry.BrokerEntryInterface;
import clientAccountInterfaces.ClientAccountInterface;
import gameInterfaces.clientGameInterfaces.ClientLobbyInterface;
import exceptions.InvalidTokenException;
import interfaces.SuccessCallbackInterface;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;

import static application.StartApplication.memoryLogger;
import static brokerEntry.BrokerEntryInterface.BROKER_HOST;
import static brokerEntry.BrokerEntryInterface.BROKER_NAME;
import static brokerEntry.BrokerEntryInterface.BROKER_PORT;
import static clientUtil.Util.saveToken;

public class StartController implements Initializable {

    // =================================================================================================================
    // fields ==========================================================================================================
    private Stage stage;
    private ClientAccountInterface clientAccountInterface;

    // =================================================================================================================
    // init ============================================================================================================

    /**
     * Method will show a loader screen, handled by {@link Loader}. This loader will block the showing of the
     * starting window untill the application has access into the network.
     *
     * @param location  default for override
     * @param resources default for override
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        // show loading screen -----------------------------------------------------------------------------------------
        final Loader loader = Loader.initStage(new Stage());
        loader.show();

        // attempt to connect into network -----------------------------------------------------------------------------
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    clientAccountInterface = ((BrokerEntryInterface)
                                    LocateRegistry.getRegistry(BROKER_HOST, BROKER_PORT).lookup(BROKER_NAME)).enter();
                } catch (RemoteException e) {
                    memoryLogger.info("could not enter memory, retrying in a bit...");
                } catch (NotBoundException e) {
                    memoryLogger.info("broker might not be running, retrying in a bit...");
                }
                if (clientAccountInterface != null) {
                    Platform.runLater(() -> {
                        loader.close();
                        stage.show();
                    });
                    this.cancel();
                }
            }
        }, 0, 5000);
    }

    /**
     * Method to initialise a new {@link StartController}. This will set the layout and styling.
     *
     * @param primaryStage  stage to put controller into
     * @return              new instance
     */
    public static StartController initStage(Stage primaryStage){
        StartController startController = null;
        try {
            FXMLLoader loader = new FXMLLoader(
                    StartController.class.getResource("../resources/views/start_view.fxml")
            );
            Scene scene = new Scene(loader.load());
            startController = loader.getController();
            scene.getStylesheets().add("resources/styles/clientStyle.css");
            primaryStage.setScene(scene);
            primaryStage.setTitle("Memory!");
            primaryStage.sizeToScene();
            primaryStage.setResizable(false);
            startController.stage = primaryStage;
        } catch (IOException e){
            e.printStackTrace();
        }
        return startController;
    }

    // =================================================================================================================
    // FXML actions ====================================================================================================

    /**
     * Method to be called when the login button is clicked. Opens a new {@link LoginController}, which blocks all
     * actions on the previously displayed {@link StartController}.
     */
    @FXML
    public void login(){
        LoginController loginController = LoginController.initStage(clientAccountInterface);
        loginController.setCallbackInterface(new SuccessCallbackInterface() {
            @Override
            public void success() {
                if(loginController.getToken() != null){
                    try {
                        saveToken(loginController.getToken());
                    } catch (FileNotFoundException e) {
                        memoryLogger.severe("problem saving token.\n");
                        e.printStackTrace();
                    }

                    LobbyController lobbyController = null;
                    try {
                        ClientLobbyInterface lobbyInterface = loginController.getLobby();
                        if (lobbyInterface != null) {
                            lobbyController = LobbyController.initStage(
                                    new Stage(),
                                    lobbyInterface
                            );
                            lobbyController.setToken(loginController.getToken());
                            stage.close();
                            lobbyController.show();
                            memoryLogger.info(String.format("logged in as %s with token %S\n",
                                    loginController.getUsername(), loginController.getToken()));
                        } else {
                            // TODO allert user ************************************************************************
                        }
                    } catch (RemoteException e) {
                        // TODO allert user ****************************************************************************
                        e.printStackTrace();
                    } catch (InvalidTokenException e) {
                        // TODO allert user ****************************************************************************
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failed() {
                memoryLogger.info("login canceled");
            }
        });
        loginController.showAndWait();
    }

    /**
     * Method to be called when the register button is clicked. Opens a new {@link RegisterController}, which blocks
     * all actions on the previously displayed {@link StartController}.
     */
    @FXML
    public void register(){
        RegisterController registerController = RegisterController.initStage(clientAccountInterface);
        registerController.setCallbackInterface(new SuccessCallbackInterface() {
            @Override
            public void success() {
                memoryLogger.info("successfully registered!\n");
            }
            @Override
            public void failed() {
                memoryLogger.info("register canceled\n");
            }
        });
        registerController.showAndWait();
    }

    /**
     * Show initialised stage.
     */
    public void show() {
        stage.show();
    }
}
