package controllers;

import interfaces.SuccessCallbackInterface;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.UUID;

import static application.StartApplication.memoryLogger;
import static gameInterfaces.dataClasses.Card.HEIGHT;
import static gameInterfaces.dataClasses.Card.WIDTH;

public class CardPickerController implements Initializable{

    private UUID cardUUID;
    private URI cardURI;
    private Stage stage;
    private HashMap<UUID, String> defaults;

    private SuccessCallbackInterface callbackInterface;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }
    public static CardPickerController initStage() {

        CardPickerController controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(LobbyController.class.getResource("../resources/views/card_picker_view.fxml"));
            Scene scene = new Scene(loader.load());
            controller = loader.getController();
            scene.getStylesheets().add("resources/styles/clientStyle.css");
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.setTitle("Memory! - card picker");
            controller.stage = stage;
        } catch (IOException e){
            e.printStackTrace();
        }
        return controller;
    }

    public UUID getCardUUID() {
        return cardUUID;
    }

    public URI getCardURI() {
        return cardURI;
    }

    public void setCallbackInterface(SuccessCallbackInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    public void show(){
        stage.show();
        defaultInit();
        stage.sizeToScene();
        stage.setResizable(false);
    }

    private void defaultInit() {
        ChoiceBox box = (ChoiceBox) stage.getScene().lookup("#options");
        ArrayList<String> options = new ArrayList<>();
        options.addAll(defaults.values());
        options.add("custom");
        box.setItems(
                FXCollections.observableArrayList(options)
        );
        box.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            String selected = box.getItems().get((int) newValue).toString();
            if(defaults.values().contains(selected)){
                cardURI = URI.create("/resources/data/" + selected);
                for(UUID key: defaults.keySet()) if(defaults.get(key).equals(selected)) cardUUID = key;
                setImage(cardURI.toString());
            }
            else find();
        });
        box.getSelectionModel().selectFirst();
    }

    private void setImage(String uri) {
        Image image = new Image(uri, WIDTH, HEIGHT, false, false);
        ((ImageView) stage.getScene().lookup("#card")).setImage(image);
        stage.setResizable(true);
        stage.sizeToScene();
        stage.setResizable(false);
    }

    private void find() {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png"),
                new FileChooser.ExtensionFilter("JPG files (*.jpg)", "*.jpg")
        );
        File file = chooser.showOpenDialog(null);
        cardURI = file.toURI();
        cardUUID = null;
        memoryLogger.info(String.format("selected file at %s", file.toURI().toString()));
        setImage(cardURI.toString());
    }

    public void cancel(MouseEvent mouseEvent) {
        callbackInterface.failed();
        stage.close();
    }

    public void accept(MouseEvent mouseEvent) {
        callbackInterface.success();
        stage.close();
    }

    public void setDefaults(HashMap<UUID, String> defaults) {
        this.defaults = defaults;
    }
}
