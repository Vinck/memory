package controllers;

import gameInterfaces.clientGameInterfaces.ClientGameInterface;
import gameInterfaces.clientGameInterfaces.HostGameInterface;
import gameInterfaces.dataClasses.Card;
import gameInterfaces.dataClasses.Message;
import exceptions.*;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import gameInterfaces.serverGameInterfaces.ClientCallbackInterface;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.rmi.RemoteException;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Timestamp;
import java.util.*;

import static application.StartApplication.memoryLogger;
import static gameInterfaces.dataClasses.Card.GAME_HEIGHT;
import static gameInterfaces.dataClasses.Card.GAME_WIDTH;

public class GameController implements Initializable, Serializable {

    @FXML private AnchorPane currentlyPlayingAnchor;
    @FXML private Label currentlyPlaying;
    @FXML private VBox listScoresSpectators;
    @FXML private ScrollPane scoresList;
    @FXML private ScrollPane spectatorList;
    @FXML private VBox chat;
    @FXML private ScrollPane chatThread;
    @FXML private TextField messageInput;
    @FXML private GridPane gamePane;

    private Stage stage;
    private String token;
    private String username;
    private ClientGameInterface gameInterface;
    private HostGameInterface hostGameInterface;

    private double scale;
    private int gameFieldWidth;

    private LinkedList<Card> cards;
    private byte[] cardBack;
    private HashMap<Integer, byte[]> tempFaces;
    private ArrayList<byte[]> faces;
    private HashMap<String, ScoreListItemController> scores;
    private HashMap<String, ScoreListItemController> spectators;

    public GameController() throws RemoteException {
    }

    class Callback extends UnicastRemoteObject implements ClientCallbackInterface {

        public Callback() throws RemoteException {
        }

        public Callback(int port) throws RemoteException {
            super(port);
        }

        public Callback(int port, RMIClientSocketFactory csf, RMIServerSocketFactory ssf) throws RemoteException {
            super(port, csf, ssf);
        }

        @Override
        public void kick() throws RemoteException {

        }

        @Override
        public void pushCards(ArrayList<byte[]> cards) throws RemoteException {
            GameController.this.faces = cards;
        }

        @Override
        public void pushCardBack(byte[] cardBack) throws RemoteException {
            GameController.this.cardBack = cardBack;
        }

        @Override
        public void receiveMessage(Message message) throws RemoteException {
            Platform.runLater(() -> {
                if(((VBox) chatThread.getContent()).getChildren().size() > 20) ((VBox) chatThread.getContent()).getChildren().remove(0);
                ((VBox) chatThread.getContent()).getChildren().add(
                        ChatBubbleController.init(message, false)
                );
            });
        }

        @Override
        public void pushField(ArrayList<Card> cards, int width, int height) throws RemoteException {
            GameController.this.cards = new LinkedList<>(cards);
            gameFieldWidth = width;
            renderField();
        }

        @Override
        public void flipCard(int card, int face) throws RemoteException {

            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(faces.get(face));
            tempFaces.put(card, faces.get(face));
            ((ImageView) gamePane.getChildren().get(card)).setImage(new Image(byteArrayInputStream, scale * GAME_WIDTH, scale * GAME_HEIGHT, false, false));
        }

        @Override
        public void correct(int first, int firstFace, int second, int secondFace) throws RemoteException {
            tempFaces.remove(first);
            cards.get(first).setFace(faces.get(firstFace));
            cards.get(first).setFlipped(true);
            tempFaces.remove(second);
            cards.get(second).setFace(faces.get(secondFace));
            cards.get(second).setFlipped(true);
        }

        @Override
        public void incorrect(int first, int second) throws RemoteException {
            tempFaces.remove(first);
            ((ImageView) gamePane.getChildren().get(first)).setImage(new Image(new ByteArrayInputStream(cardBack), scale * GAME_WIDTH, scale * GAME_HEIGHT, false, false));
            tempFaces.remove(second);
            ((ImageView) gamePane.getChildren().get(second)).setImage(new Image(new ByteArrayInputStream(cardBack), scale * GAME_WIDTH, scale * GAME_HEIGHT, false, false));
        }

        @Override
        public void play() throws RemoteException {
            Platform.runLater(() -> {
                currentlyPlaying.setText("you are now playing");
                currentlyPlaying.getStyleClass().remove("currently_playing_false");
                currentlyPlaying.getStyleClass().remove("currently_playing_waiting");
                currentlyPlaying.getStyleClass().add("currently_playing_true");
                currentlyPlayingAnchor.getStyleClass().remove("currently_playing_false");
                currentlyPlayingAnchor.getStyleClass().remove("currently_playing_waiting");
                currentlyPlayingAnchor.getStyleClass().add("currently_playing_true");
            });
        }

        @Override
        public void stopPlaying() throws RemoteException {
            // deprecated -> updatePlaying(String)
        }

        @Override
        public void updateScoreList(String username, int score) throws RemoteException {
            if(!GameController.this.scores.keySet().contains(username)){
                GameController.this.scores.put(
                        username,
                        ScoreListItemController.init(username, score)
                );
                Platform.runLater(() -> {
                    ((VBox) scoresList.getContent()).getChildren().add(scores.get(username).getNode());
                    stage.sizeToScene();
                });
            } else {
                Platform.runLater(() -> {
                    scores.get(username).update(score);
                    stage.sizeToScene();
                });
            }
        }

        @Override
        public void updatePlaying(String username) throws RemoteException {
            if(username != null){
                for(ScoreListItemController controller: scores.values()){
                    controller.setPlaying(false);
                }
                scores.get(username).setPlaying(true);

                Platform.runLater(() -> {
                    currentlyPlaying.setText(String.format("%s is now playing", username));
                    currentlyPlaying.getStyleClass().remove("currently_playing_false");
                    currentlyPlaying.getStyleClass().remove("currently_playing_waiting");
                    currentlyPlaying.getStyleClass().add("currently_playing_false");
                    currentlyPlayingAnchor.getStyleClass().remove("currently_playing_false");
                    currentlyPlayingAnchor.getStyleClass().remove("currently_playing_waiting");
                    currentlyPlayingAnchor.getStyleClass().add("currently_playing_false");
                });
            } else {
                for(ScoreListItemController controller: scores.values()){
                    controller.setPlaying(false);
                }

                Platform.runLater(() -> {
                    currentlyPlaying.setText("waiting for players...");
                    currentlyPlaying.getStyleClass().remove("currently_playing_false");
                    currentlyPlaying.getStyleClass().remove("currently_playing_true");
                    currentlyPlaying.getStyleClass().add("currently_playing_waiting");
                    currentlyPlayingAnchor.getStyleClass().remove("currently_playing_false");
                    currentlyPlayingAnchor.getStyleClass().remove("currently_playing_true");
                    currentlyPlayingAnchor.getStyleClass().add("currently_playing_waiting");
                });
            }
        }

        @Override
        public void winner(List<String> winners, int score) throws RemoteException {
            for (String username: scores.keySet()){
                if(winners.contains(username)) scores.get(username).setWinner();
            }
            Platform.runLater(() -> {
                currentlyPlaying.setText("game finished");
                currentlyPlaying.getStyleClass().remove("currently_playing_true");
                currentlyPlaying.getStyleClass().remove("currently_playing_false");
                currentlyPlaying.getStyleClass().add("currently_playing_waiting");
                currentlyPlayingAnchor.getStyleClass().remove("currently_playing_true");
                currentlyPlayingAnchor.getStyleClass().remove("currently_playing_false");
                currentlyPlayingAnchor.getStyleClass().add("currently_playing_waiting");
            });
        }

        @Override
        public void updateSpectators(String username, boolean spectating) throws RemoteException{
            if(!spectators.keySet().contains(username) && spectating){
                spectators.put(
                        username,
                        ScoreListItemController.init(username, -1)
                );
                Platform.runLater(() -> {
                    ((VBox) spectatorList.getContent()).getChildren().add(spectators.get(username).getNode());
                    stage.sizeToScene();
                });
            } else if(spectators.containsKey(username) && !spectating){
                Platform.runLater(() -> {
                    ((VBox) spectatorList.getContent()).getChildren().remove(spectators.get(username).getNode());
                    stage.sizeToScene();
                    spectators.remove(username);
                });
            }
        }
    }
    private ClientCallbackInterface clientCallbackInterface = new Callback();

    private void renderField() {
        gamePane.getChildren().clear();
        gamePane.setHgap(scale * 20);
        gamePane.setVgap(scale * 20);

        ByteArrayInputStream cardStream;

        try {
            for (Card card : cards) {
                final int index = cards.indexOf(card);
                if (card.getFace() == null) {
                    cardStream = new ByteArrayInputStream(cardBack);
                    ImageView imageView = new ImageView(new Image(cardStream, scale * GAME_WIDTH, scale * GAME_HEIGHT, false, false));
                    cardStream.close();
                    imageView.setOnMouseClicked(event -> {
                        try {
                            if(!cards.get(index).isFlipped()) {
                                gameInterface.selectCard(token, index);
                            }
                        } catch (NotYourTurnException e) {
                            //TODO something
                        } catch (RemoteException e) {
                            memoryLogger.severe("something went wrong at the server side...\n");
                            e.printStackTrace();
                        } catch (NotenoughPlayersException e) {
                            memoryLogger.info("not enough players.\n");
                        }
                    });
                    gamePane.add(imageView, index % gameFieldWidth, index / gameFieldWidth);
                } else {
                    if(tempFaces.keySet().contains(index)) cardStream = new ByteArrayInputStream(tempFaces.get(index));
                    else cardStream = new ByteArrayInputStream(card.getFace());

                    ImageView imageView = new ImageView(new Image(cardStream, scale * GAME_WIDTH, scale * GAME_HEIGHT, false, false));
                    cardStream.close();
                    gamePane.add(imageView, index % gameFieldWidth, index / gameFieldWidth);
                }
            }
        } catch (IOException e){
            // geen idee
        }
        Platform.runLater(() -> {
            stage.setResizable(true);
            stage.sizeToScene();
            stage.setResizable(false);
        });
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ((VBox) chatThread.getContent()).heightProperty().addListener(observable -> chatThread.setVvalue(1.0));

        ContextMenu contextMenu = new ContextMenu();
        MenuItem item1 = new MenuItem("scale 50%");
        item1.setOnAction(actionEvent -> {
            scale = 0.5;
            renderField();
        });
        MenuItem item2 = new MenuItem("scale 75%");
        item2.setOnAction(actionEvent -> {
            scale = 0.75;
            renderField();
        });
        MenuItem item3 = new MenuItem("scale 100%");
        item3.setOnAction(actionEvent -> {
            scale = 1.0;
            renderField();
        });
        contextMenu.getItems().addAll(item1, item2, item3);
        gamePane.setOnContextMenuRequested(event -> contextMenu.show(gamePane, event.getScreenX(), event.getScreenY()));
    }

    public static GameController initStage() {

        GameController controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(GameController.class.getResource("../resources/views/game_view.fxml"));
            Scene scene = new Scene(loader.load());
            controller = loader.getController();
            scene.getStylesheets().add("resources/styles/clientStyle.css");
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.setWidth(500);
            stage.setHeight(1000);
            stage.setTitle("Memory!");
            stage.sizeToScene();
            stage.setResizable(false);
            controller.stage = stage;
            controller.scores = new HashMap<>();
            controller.spectators = new HashMap<>();
            controller.scale = 1.0;
            controller.tempFaces = new HashMap<>();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return controller;
    }

    public void show() {
        stage.setOnCloseRequest((WindowEvent event) -> {
            try {
                gameInterface.logout(token, GameController.this.clientCallbackInterface);
            } catch (RemoteException e) {
                e.printStackTrace();
            } finally {
                stage.close();
            }
        });
        stage.show();
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setGameInterface(ClientGameInterface gameInterface) {
        this.gameInterface = gameInterface;
    }

    public void setHostGameInterface(HostGameInterface hostGameInterface) {
        this.hostGameInterface = hostGameInterface;
    }

    public ClientCallbackInterface getClientCallbackInterface() {
        return clientCallbackInterface;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void join() {
        try {
            gameInterface.join(token, username, clientCallbackInterface);
            show();
        } catch (RemoteException e) {
            memoryLogger.info("problem with application server.");
            e.printStackTrace();
        } catch (InvalidTokenException e) {
            memoryLogger.info("token no longer valid.\n");
            e.printStackTrace();
        } catch (FullGameException e) {
            memoryLogger.info("game is already full.\n");
            stage.close();
        } catch (GameNotOpenException e) {
            memoryLogger.info("game not open.\n");
        }
    }

    public void spectate() {
        try {
            gameInterface.spectate(token, username, clientCallbackInterface);
        } catch (RemoteException e) {
            memoryLogger.info("problem with application server.");
            e.printStackTrace();
        }
    }

    public void open() {
        try {
            hostGameInterface.open(token, username, clientCallbackInterface);
            show();
        } catch (RemoteException e) {
            memoryLogger.info("problem with application server.");
            e.printStackTrace();
        } catch (NotEnoughCardsException e) {
            memoryLogger.info("not enough cards in game.\n");
            e.printStackTrace();
        } catch (InvalidTokenException e) {
            memoryLogger.info("token no longer valid.\n");
            e.printStackTrace();
        }
    }

    public void sendMessage(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.ENTER) {
           sendMessageButtonAction(null);
        }
    }

    public void sendMessageButtonAction(MouseEvent mouseEvent) {
        try {
            String message = messageInput.getText();
            gameInterface.sendMessage(
                    token,
                    message
            );
            ((VBox) chatThread.getContent()).getChildren().add(ChatBubbleController.init(
                    new Message(
                            username,
                            message,
                            new Timestamp(new Date().getTime())
                    ),
                    true
            ));
            messageInput.setText("");
        } catch (RemoteException e) {
            memoryLogger.info("problem with server.\n");
            e.printStackTrace();
        }
    }
}