package controllers;

import gameInterfaces.dataClasses.Message;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;

import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;

import static application.StartApplication.memoryLogger;

public class ChatBubbleController implements Initializable{

    @FXML private Label username;
    @FXML private Label message;
    @FXML private Label timestamp;

    private SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public static Node init(Message message, boolean own){

        Node node = null;
        ChatBubbleController controller;

        try{
            FXMLLoader loader = new FXMLLoader(ChatBubbleController.class.getResource(
                    own ? "/resources/views/components/chat_bubble_own.fxml" : "/resources/views/components/chat_bubble.fxml")
            );
            node = loader.load();
            controller = loader.getController();
            if(!own) controller.username.setText(message.getUsername());
            controller.message.setText(message.getMessage());
            controller.timestamp.setText(controller.formatter.format(message.getTime()));
        } catch (IOException e) {
            memoryLogger.info("problem loading chat bubble.\n");
            e.printStackTrace();
        }
        return node;
    }
}
