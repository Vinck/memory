package controllers;

import clientAccountInterfaces.ClientAccountInterface;
import gameInterfaces.clientGameInterfaces.ClientLobbyInterface;
import exceptions.AccountNotFoundException;
import exceptions.InvalidTokenException;
import exceptions.WrongPasswordException;
import interfaces.SuccessCallbackInterface;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

import static application.StartApplication.memoryLogger;

public class LoginController implements Initializable{

    // =================================================================================================================
    // FXML fields =====================================================================================================
    @FXML private TextField usernameField;
    @FXML private PasswordField passwordField;

    // =================================================================================================================
    // fields ==========================================================================================================
    private Stage stage;
    private SuccessCallbackInterface callbackInterface;
    private String username;
    private String password;
    private String token;

    private ClientAccountInterface clientAccountInterface;

    // =================================================================================================================
    // init ============================================================================================================

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usernameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(usernameField.getText().isEmpty()) usernameField.getStyleClass().add("text_field_error");
            else usernameField.getStyleClass().remove("text_field_error");
        });
        usernameField.onKeyPressedProperty().set(event -> {
            if(event.getCode() == KeyCode.ENTER) passwordField.requestFocus();
        });

        passwordField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(passwordField.getText().isEmpty()) passwordField.getStyleClass().add("text_field_error");
            else passwordField.getStyleClass().remove("text_field_error");
        });
        passwordField.onKeyPressedProperty().set((event -> {
            if(event.getCode() == KeyCode.ENTER) login();
        }));
    }

    /**
     * Method to get new instance of the {@link LoginController}. This method will load the layout for the window and
     * and set the connection to the game server.
     *
     * @param clientAccountInterface    previously acquired connection to a game server
     * @return                          a new instance of the controller
     */
    public static LoginController initStage(ClientAccountInterface clientAccountInterface){
        LoginController controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(
                    LoginController.class.getResource("../resources/views/login_view.fxml")
            );
            Scene parent = new Scene(loader.load());
            controller = loader.getController();
            controller.stage = new Stage();
            controller.clientAccountInterface = clientAccountInterface;
            parent.getStylesheets().add("resources/styles/clientStyle.css");
            controller.stage.initModality(Modality.APPLICATION_MODAL);
            controller.stage.setTitle("Memory! - login");
            controller.stage.setScene(parent);
            controller.stage.sizeToScene();
            controller.stage.setResizable(false);
        } catch (IOException e){
            e.printStackTrace();
        }
        return controller;
    }

    // =================================================================================================================
    // FXML methods ====================================================================================================
    @FXML
    public void clear(Event event) {
        ((TextField) event.getSource()).getStyleClass().remove("text_field_error");
    }

    @FXML
    public void login(){
        username = usernameField.getText();
        password = passwordField.getText();
        try {
            token = clientAccountInterface.login(username, password);
            stage.close();
            callbackInterface.success();
        } catch (RemoteException e) {
            memoryLogger.severe("unable to communicate with application server");
            e.printStackTrace();
        } catch (AccountNotFoundException e) {
            ((Label) stage.getScene().lookup("#error_label")).setText("username not found");
            passwordField.setText("");
            usernameField.getStyleClass().add("text_field_error");
        } catch (WrongPasswordException e) {
            ((Label) stage.getScene().lookup("#error_label")).setText("password does not match");
            passwordField.getStyleClass().add("text_field_error");
            passwordField.setText("");
        }
    }

    // =================================================================================================================
    // FXML related methods ============================================================================================

    @FXML
    public void cancel(){
        stage.close();
    }

    public void showAndWait() {
        this.stage.showAndWait();
    }

    // =================================================================================================================
    // getters & setters ===============================================================================================
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setCallbackInterface(SuccessCallbackInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    public String getToken() {
        return token;
    }

    public ClientLobbyInterface getLobby() throws RemoteException, InvalidTokenException {
        return clientAccountInterface.getLobby(token);
    }
}
