package controllers;

import clientAccountInterfaces.ClientAccountInterface;
import exceptions.EmailAddressAlreadyInUseException;
import exceptions.UsernameAlreadyExistsException;
import interfaces.SuccessCallbackInterface;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ResourceBundle;

import static application.StartApplication.memoryLogger;

public class RegisterController implements Initializable{

    // =================================================================================================================
    // FXML fields =====================================================================================================

    @FXML private TextField usernameField;
    @FXML private PasswordField passwordField;
    @FXML private PasswordField passwordConfirmField;
    @FXML private TextField emailField;

    // =================================================================================================================
    // fields ==========================================================================================================

    private Stage stage;
    private SuccessCallbackInterface callbackInterface;
    private String username;
    private String password;
    private String email;
    private ClientAccountInterface clientAccountInterface;

    // =================================================================================================================
    // init ============================================================================================================

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        usernameField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.isEmpty()) usernameField.getStyleClass().add("text_field_error");
        });
        usernameField.onKeyPressedProperty().set((event -> {
            if(event.getCode() == KeyCode.ENTER) passwordField.requestFocus();
        }));

        passwordField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.isEmpty()) passwordField.getStyleClass().add("text_field_error");
        });
        passwordField.onKeyPressedProperty().set(event -> {
            if(event.getCode() == KeyCode.ENTER) passwordConfirmField.requestFocus();
        });

        passwordConfirmField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.isEmpty()) passwordConfirmField.getStyleClass().add("text_field_error");
        });
        passwordConfirmField.onKeyPressedProperty().set(event -> {
            if(event.getCode() == KeyCode.ENTER) emailField.requestFocus();
        });

        emailField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue.isEmpty()) emailField.getStyleClass().add("text_field_error");
        });
        emailField.onKeyPressedProperty().set(event -> {
            if(event.getCode() == KeyCode.ENTER) register();
        });
    }

    /**
     * Method to get new intance of the {@link RegisterController}. This method will handle the loading of the
     * layout, as well as all the needed setup. The Previously acquired {@link ClientAccountInterface} is also
     * passed to the {@link RegisterController}.
     *
     * @param clientAccountInterface    previously acquired connection to a game server
     * @return                          a new instance of the controller
     */
    public static RegisterController initStage(ClientAccountInterface clientAccountInterface){
        RegisterController registerController = null;
        try {
            FXMLLoader loader = new FXMLLoader(
                    RegisterController.class.getResource("../resources/views/register_view.fxml")
            );
            Scene scene = new Scene(loader.load());
            registerController = loader.getController();
            registerController.stage = new Stage();
            registerController.clientAccountInterface = clientAccountInterface;
            scene.getStylesheets().add("resources/styles/clientStyle.css");
            registerController.stage.initModality(Modality.APPLICATION_MODAL);
            registerController.stage.setTitle("Memory! - register");
            registerController.stage.setScene(scene);
            registerController.stage.sizeToScene();
            registerController.stage.setResizable(false);
        } catch (IOException e){
            memoryLogger.severe("problem reading layout file");
            e.printStackTrace();
        }
        return registerController;
    }

    // =================================================================================================================
    // FXML methods ====================================================================================================

    /**
     * Method used to clear an error styling on a text field when it is clicked.
     *
     * @param event default parameter
     */
    @FXML
    public void clear(Event event) {
        ((TextField) event.getSource()).getStyleClass().remove("text_field_error");
    }

    /**
     * Method used to confirm the register request. All values will be checked client side to relieve unnecessary
     * network overhead for faulty requests. The requirements are:
     * <ul>
     *     <li>username cannot be empty</li>
     *     <li>password cannot be empty</li>
     *     <li>password confirm cannot be empty</li>
     *     <li>password and password confirm need to match</li>
     *     <li>email cannot be empty</li>
     * </ul>
     *
     * In case an exception is thrown by the server when attempting to register at the server.
     */
    @FXML
    public void register(){

        // acquire values from text fields -----------------------------------------------------------------------------
        username = usernameField.getText();
        password = passwordField.getText();
        String passwordConfirm = passwordConfirmField.getText();
        email = emailField.getText();

        // check for unfilled text fields ------------------------------------------------------------------------------
        if(username.isEmpty() || password.isEmpty() || email.isEmpty() || passwordConfirm.isEmpty()){
            // set error label and set error styling on empty text fields ----------------------------------------------
            ((Label) stage.getScene().lookup("#error_label")).setText("Please fill in all fields");
            if(username.isEmpty()) usernameField.getStyleClass().add("text_field_error");
            if(password.isEmpty()) passwordField.getStyleClass().add("text_field_error");
            if(passwordConfirm.isEmpty()) passwordConfirmField.getStyleClass().add("text_field_error");
            if(email.isEmpty()) emailField.getStyleClass().add("text_field_error");

        }
        // check if password matches confrim password ------------------------------------------------------------------
        else if(!password.equals(passwordConfirm)){
            // set error label and set error styling on both password fields -------------------------------------------
            ((Label) stage.getScene().lookup("#error_label")).setText("Passwords don't match");
            passwordField.getStyleClass().add("text_field_error");
            passwordConfirmField.getStyleClass().add("text_field_error");
        }
        // send register request to server -----------------------------------------------------------------------------
        else {
            try {
                if (clientAccountInterface.register(username, password, email)) {
                    callbackInterface.success();
                    stage.close();
                } else {
                    ((Label) stage.getScene().lookup("#error_label")).setText("sorry, server issues...");
                    memoryLogger.info("somthing went wrong server side...");
                }
            } catch (RemoteException e) {
                memoryLogger.severe("problem connecting to broker");
                e.printStackTrace();
            } catch (UsernameAlreadyExistsException e) {
                memoryLogger.info(String.format("user %s already exists.", username));
                usernameField.getStyleClass().add("text_field_error");
                ((Label) stage.getScene().lookup("#error_label")).setText("username already in use");
            } catch (EmailAddressAlreadyInUseException e) {
                memoryLogger.info(String .format("email address %s already in use", email));
                emailField.getStyleClass().add("text_field_error");
                ((Label) stage.getScene().lookup("#error_label")).setText("email address already in use");
            }
        }

        // clear password fields to hide password ----------------------------------------------------------------------
        passwordField.setText("");
        passwordConfirmField.setText("");
    }

    /**
     * FXML method to call when the cancel button is clicked. Simple method to close the window.
     */
    @FXML
    public void cancel(){
        stage.close();
    }

    public void showAndWait() {
        this.stage.showAndWait();
    }

    // =================================================================================================================
    // getters & setters ===============================================================================================

    public void setCallbackInterface(SuccessCallbackInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public String getEmail() {
        return this.email;
    }

}
