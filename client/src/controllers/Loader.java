package controllers;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Loader implements Initializable {

    // =================================================================================================================
    // FXML fields =====================================================================================================
    private Stage stage;

    // =================================================================================================================
    // fields ==========================================================================================================

    // =================================================================================================================
    // init ============================================================================================================

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public static Loader initStage(Stage primaryStage) {
        Loader controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(
                    StartController.class.getResource("../resources/views/loader_view.fxml")
            );
            Scene scene = new Scene(loader.load());
            controller = loader.getController();
            scene.getStylesheets().add("resources/styles/clientStyle.css");
            primaryStage.setScene(scene);
            primaryStage.sizeToScene();
            primaryStage.setResizable(false);
            primaryStage.setAlwaysOnTop(true);
            primaryStage.initModality(Modality.APPLICATION_MODAL);
            controller.stage = primaryStage;
        } catch (IOException e){
            e.printStackTrace();
        }
        return controller;
    }

    // =================================================================================================================
    // methods =========================================================================================================

    public void show() {
        stage.show();
    }

    public void close() {
        stage.close();
    }
}
