package controllers;

import gameInterfaces.dataClasses.GameInfo;
import interfaces.SuccessCallbackInterface;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class LobbyListItemController implements Initializable{

    @FXML private Label hostName;
    @FXML private Label players;
    @FXML private Label limit;
    @FXML private Label name;

    private GameInfo gameInfo;
    private SuccessCallbackInterface successCallbackInterface;

    public LobbyListItemController(GameInfo gameInfo, SuccessCallbackInterface callbackInterface) {
        this.gameInfo = gameInfo;
        this.successCallbackInterface = callbackInterface;
    }

    public static Node init(GameInfo gameInfo, SuccessCallbackInterface callbackInterface){

        FXMLLoader loader = new FXMLLoader(LobbyListItemController.class.getResource("/resources/views/components/lobby_game_component.fxml"));
        loader.setControllerFactory(param -> new LobbyListItemController(gameInfo, callbackInterface));
        Node node = null;
        try {
            node = loader.load();
            ((LobbyListItemController) loader.getController()).update();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return node;
    }

    private void update() {
        name.setText(gameInfo.getName());
        limit.setText(String.valueOf(gameInfo.getLimit()));
        players.setText(String.valueOf(gameInfo.getPlayers()));
        hostName.setText(gameInfo.getHost());
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void join(MouseEvent event) {
        successCallbackInterface.success();
    }

    public void spectate(MouseEvent mouseEvent) {
        successCallbackInterface.failed();
    }
}
