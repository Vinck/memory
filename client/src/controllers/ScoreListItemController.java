package controllers;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class ScoreListItemController implements Initializable {

    @FXML private HBox root;
    @FXML private Label username;
    @FXML private Label score;

    private Node node;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public static ScoreListItemController init(String username, int score){

        ScoreListItemController controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(LoginController.class.getResource("../resources/views/components/score_list_item.fxml"));
            loader.load();
            controller = loader.getController();
            controller.node = loader.getRoot();
            controller.username.setText(username);
            controller.score.setText(score >= 0 ? String.valueOf(score) : "");
        } catch (IOException e){
            e.printStackTrace();
        }
        return controller;
    }

    public void update(int score){
        this.score.setText(String.valueOf(score));
    }

    public Node getNode() {
        return node;
    }

    public void setPlaying(boolean playing){
        if(playing){
            root.getStyleClass().add("score_list_playing");
            username.getStyleClass().add("score_list_username_playing");
        } else {
            root.getStyleClass().remove("score_list_playing");
            username.getStyleClass().remove("score_list_username_playing");
        }
    }

    public void setWinner() {
        root.getStyleClass().remove("score_list_playing");
        username.getStyleClass().remove("score_list_username_playing");

        root.getStyleClass().add("score_list_winner");
        username.getStyleClass().add("score_list_username_winner");
        score.getStyleClass().add("score_list_score_winner");
    }
}
