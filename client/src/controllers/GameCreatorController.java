package controllers;

import interfaces.SuccessCallbackInterface;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.List;

import static application.StartApplication.memoryLogger;
import static gameInterfaces.dataClasses.Card.HEIGHT;
import static gameInterfaces.dataClasses.Card.WIDTH;
import static gameInterfaces.util.Util.rescaleGetBytes;

public class GameCreatorController implements Initializable {

    // =================================================================================================================
    // static fields ===================================================================================================
    private static final int DEFAULT_LIMIT = 2;
    private static final int DEFAULT_WIDTH = 4;
    private static final int DEFAULT_HEIGHT = 3;

    // =================================================================================================================
    // FXML fields =====================================================================================================
    @FXML private TextField nameField;
    @FXML private TextField widthField;
    @FXML private TextField heightField;
    @FXML private TextField playerLimitField;
    @FXML private HBox facesBox;

    // =================================================================================================================
    // other fields ====================================================================================================
    private Stage stage;
    private SuccessCallbackInterface callbackInterface;

    private String name;
    private int width;
    private int height;
    private int limit;
    private ArrayList<URI> faces;
    private ArrayList<UUID> standardFaces;
    private URI back;
    private ArrayList<byte[]> facesBytes;
    private HashMap<UUID, String> defaults;

    // =================================================================================================================
    // init ============================================================================================================

    /**
     * {@link Initializable} override. Default values for the game are set in both the class' fields as well as in the
     * layout.
     *
     * @param location  default for override
     * @param resources default for override
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        faces = new ArrayList<>((DEFAULT_WIDTH * DEFAULT_HEIGHT) / 2);
        standardFaces = new ArrayList<>((DEFAULT_WIDTH * DEFAULT_HEIGHT) / 2);

        width = DEFAULT_WIDTH;
        height = DEFAULT_HEIGHT;
        limit = DEFAULT_LIMIT;

        try {
            defaults = new HashMap<>();
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(getClass().getResourceAsStream("/resources/data/defaults"), StandardCharsets.UTF_8)
            );
            String line;
            while ((line = reader.readLine()) != null){
                defaults.put(
                        UUID.fromString(line.split(",")[1]),
                        line.split(",")[0]
                );
            }
        } catch (FileNotFoundException e) {
            memoryLogger.severe("error reading default cards.\n");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method to initialise a new {@link GameController}. This will set the layout for the window and the styling.
     *
     * @return  new instance
     */
    public static GameCreatorController initStage() {

        GameCreatorController controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(LobbyController.class.getResource("../resources/views/game_creator_view.fxml"));
            Scene scene = new Scene(loader.load());
            controller = loader.getController();
            scene.getStylesheets().add("resources/styles/clientStyle.css");
            Stage stage = new Stage();
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setScene(scene);
            stage.setTitle("Memory! - game creator");
            ((ImageView) stage.getScene().lookup("#card_back")).setImage(new Image(
                    "/resources/data/MemoryStandardCardBack.png"
            ));

            controller.stage = stage;
        } catch (IOException e){
            e.printStackTrace();
        }
        return controller;
    }

    public void show(){

        nameField.textProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if(newValue.equals("")){
                        nameField.getStyleClass().add("text_field_error");
                    } else {
                        nameField.getStyleClass().remove("text_field_error");
                    }
                    name = newValue;
                });

        widthField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if(newValue.equals("")){
                        widthField.getStyleClass().add("text_field_error");
                    } else {
                        try {
                            width = Integer.parseInt(newValue);
                            updateSize();
                            widthField.getStyleClass().remove("text_field_error");
                        } catch (NumberFormatException e) {
                            widthField.setText(Integer.toString(width));
                        }
                    }
                }
                ));

        heightField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if(newValue.equals("")){
                        heightField.getStyleClass().add("text_field_error");
                    } else {
                        try {
                            height = Integer.parseInt(newValue);
                            heightField.getStyleClass().remove("text_field_error");
                            updateSize();
                        } catch (NumberFormatException e) {
                            heightField.setText(Integer.toString(height));
                        }
                    }
                }
                ));

        playerLimitField.textProperty().addListener(
                ((observable, oldValue, newValue) -> {
                    if(newValue.equals("")){
                        playerLimitField.getStyleClass().add("text_field_error");
                    } else {
                        try {
                            limit = Integer.parseInt(newValue);
                            playerLimitField.getStyleClass().remove("text_field_error");
                        } catch (NumberFormatException e) {
                            playerLimitField.setText(Integer.toString(limit));
                        }
                    }
                }
                ));


        stage.show();
        stage.sizeToScene();
        updateSize();
        stage.setResizable(false);
        resetDefault(null);
    }

    // =================================================================================================================
    // getters and setters =============================================================================================

    public void setCallbackInterface(SuccessCallbackInterface callbackInterface) {
        this.callbackInterface = callbackInterface;
    }

    public String getName() {
        return name;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getLimit() {
        return limit;
    }

    public List<URI> getFaces() {
        return faces;
    }

    public URI getBack() {
        return back;
    }

    public List<UUID> getStandardFaces() {
        return standardFaces;
    }

    public ArrayList<byte[]> getFacesBytes() {
        return facesBytes;
    }

    public void updateDimensions(KeyEvent inputMethodEvent) {
        if(inputMethodEvent.getCode() == KeyCode.ENTER) updateSize();
    }

    // =================================================================================================================
    // FWML and related methods ========================================================================================

    /**
     * Method to update the fields related to the game field's dimensions. Depending on the game field's dimensions
     * more or less card will be required. This will update the lists and the layout to match the needed amount of
     * cards.
     */
    private void updateSize() {
        memoryLogger.info(String.format("dimensions are: %dx%d", width, height));
        int count = (width*height)/2;

        // size has been decreased -------------------------------------------------------------------------------------
        if(facesBox.getChildren().size() > count){
            memoryLogger.info("changing dimensions");
            while (standardFaces.size() > count) standardFaces.remove(count);
            while (faces.size() > count) faces.remove(count);
            standardFaces.trimToSize();
            faces.trimToSize();
            facesBox.getChildren().remove(count, facesBox.getChildren().size());
        }
        // size decreased ----------------------------------------------------------------------------------------------
        else if(facesBox.getChildren().size() < count){
            memoryLogger.info("changing dimensions");
            ImageView view;
            for(int i = facesBox.getChildren().size(); i < count; i++){
                view = new ImageView();
                view.setImage(new Image("/resources/data/MemoryStandardCardBack.png"));
                view.setOnMouseClicked(this::pickCard);
                facesBox.getChildren().add(view);
            }
            while (faces.size() < count) faces.add(null);
            while (standardFaces.size() < count) standardFaces.add(null);
        }
        memoryLogger.info(String.format("faces:\t%s", faces));
        memoryLogger.info(String.format("standards:\t%s", standardFaces));
    }

    /**
     * Method to handle mouse event to select a new card face or back. This will open a new window with a
     * {@link CardPickerController}.
     *
     * @param mouseEvent    default to handle click action
     */
    public void pickCard(MouseEvent mouseEvent) {
        ImageView source = (ImageView) mouseEvent.getSource();
        CardPickerController cardPickerController = CardPickerController.initStage();
        cardPickerController.setCallbackInterface(new SuccessCallbackInterface() {
            @Override
            public void success() {
                int index = facesBox.getChildren().indexOf(mouseEvent.getSource());
                if(cardPickerController.getCardUUID() != null){
                    memoryLogger.info(String.format("selected UUID: %s", cardPickerController.getCardUUID()));
                    standardFaces.set(index, cardPickerController.getCardUUID());
                    source.setImage(new Image(cardPickerController.getCardURI().toString()));
                } else {
                    faces.set(index, cardPickerController.getCardURI());
                }
                Image image = new Image(cardPickerController.getCardURI().toString(), WIDTH, HEIGHT, false, false);
                source.setImage(image);
            }

            @Override
            public void failed() {
                memoryLogger.info("no card selected.\n");
            }
        });
        HashMap<UUID, String> newDefault = new HashMap<>(defaults);
        newDefault.keySet().removeAll(standardFaces);
        cardPickerController.setDefaults(newDefault);
        cardPickerController.show();
    }

    /**
     * Method to reset to the default values for the game field size, player limit as well as all card faces and the
     * card back.
     *
     * @param mouseEvent    default for mouse click handling
     */
    public void resetDefault(MouseEvent mouseEvent) {
        width = DEFAULT_WIDTH;
        widthField.setText(String.valueOf(width));
        height = DEFAULT_HEIGHT;
        heightField.setText(String.valueOf(height));
        limit = DEFAULT_LIMIT;
        playerLimitField.setText(String.valueOf(limit));
        back = null;
        faces = new ArrayList<>((width * height) / 2);
        standardFaces = new ArrayList<>((width * height) / 2);
        updateSize();
        int i = 0;
        List<Node> UIFaces = facesBox.getChildren();
        for(UUID key: defaults.keySet()){
            if(!key.toString().equals("00000007-0000-0000-0000-000000000000")){
                ((ImageView) UIFaces.get(i)).setImage(new Image("/resources/data/"+ defaults.get(key)));
                standardFaces.add(key);
                faces.add(null);
                i++;
            }
        }
        ((ImageView) stage.getScene().lookup("#card_back")).setImage(new Image("/resources/data/MemoryStandardCardBack.png"));

        memoryLogger.info(String.format("faces:\t%s", faces));
        memoryLogger.info(String.format("standards:\t%s", standardFaces));
    }

    /**
     * Method to open new window to select new card back. This will open a new window with the
     * {@link CardPickerController}.
     *
     * @param mouseEvent    defaul to handle mouse click
     */
    public void pickBack(MouseEvent mouseEvent) {
        ImageView source = (ImageView) mouseEvent.getSource();
        CardPickerController cardPickerController = CardPickerController.initStage();
        cardPickerController.setCallbackInterface(new SuccessCallbackInterface() {
            @Override
            public void success() {
                if(cardPickerController.getCardUUID() != null){
                    memoryLogger.info(String.format("selected UUID: %s", cardPickerController.getCardUUID()));
                    back = null;
                } else {
                    back = cardPickerController.getCardURI();
                }
                Image image = new Image(cardPickerController.getCardURI().toString(), WIDTH, HEIGHT, false, false);
                source.setImage(image);
            }

            @Override
            public void failed() {
                memoryLogger.info("no card selected.\n");
            }
        });
        HashMap<UUID, String> newDefaults = new HashMap<>();
        newDefaults.put(UUID.fromString("0000000-0000-0000-0000-000000000000"), "MemoryStandardCardBack.png");
        cardPickerController.setDefaults(newDefaults);
        cardPickerController.show();
    }

    /**
     * Method to clear styling on text field when clicked.
     *
     * @param mouseEvent    default to handle mouse click
     */
    public void clear(MouseEvent mouseEvent) {
        ((TextField) mouseEvent.getSource()).getStyleClass().remove("text_field_error");
    }

    /**
     * Method to confirm the provided specifications for a new game. The selected card faces and card back will be
     * checked and the request for a new game will be set to the application server.
     *
     * @param mouseEvent    default to handle mouse click
     */
    public void create(MouseEvent mouseEvent) {
        memoryLogger.info("checking game...");
        this.facesBytes = new ArrayList<>();
        try {
            // check for empty name ------------------------------------------------------------------------------------
            if(name == null || name.equals("")){
                stage.getScene().lookup("#name").getStyleClass().add("text_field_error");
                ((Label) stage.getScene().lookup("#error_label")).setText("please provide an original name.");
                memoryLogger.info("no name provided.");
            }
            else {
                // check if all faces are provided ---------------------------------------------------------------------
                // TODO check duplicates *******************************************************************************
                for (int i = 0; i < faces.size(); i++) {
                    if (faces.get(i) == null && standardFaces.get(i) == null) {
                        memoryLogger.info("not all images given.");
                        ((Label) stage.getScene().lookup("#error_label"))
                                .setText("please provide all necessary faces.");
                        return;
                    } else if (faces.get(i) != null) {
                        standardFaces.set(i, null);
                        facesBytes.add(rescaleGetBytes(faces.get(i).getPath()));
                    }
                }
                stage.close();
                callbackInterface.success();
            }
        } catch (IOException e) {
            memoryLogger.severe("problem reading card face.\n");
            e.printStackTrace();
        }
    }

    /**
     * Method to cancel game creation.
     *
     * @param mouseEvent    default to handle mouse click
     */
    public void cancel(MouseEvent mouseEvent) {
        stage.close();
        callbackInterface.failed();
    }
}
