package controllers;

import brokerEntry.BrokerEntryInterface;
import gameInterfaces.clientGameInterfaces.ClientGameInterface;
import gameInterfaces.clientGameInterfaces.ClientLobbyInterface;
import gameInterfaces.clientGameInterfaces.HostGameInterface;
import exceptions.InvalidTokenException;
import gameInterfaces.serverGameInterfaces.ClientCallbackInterface;
import interfaces.SuccessCallbackInterface;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.*;

import static application.StartApplication.memoryLogger;
import static brokerEntry.BrokerEntryInterface.BROKER_HOST;
import static brokerEntry.BrokerEntryInterface.BROKER_NAME;
import static brokerEntry.BrokerEntryInterface.BROKER_PORT;
import static gameInterfaces.serverGameInterfaces.ClientCallbackInterface.CLIENT_GAME_NAME;
import static gameInterfaces.serverGameInterfaces.ClientCallbackInterface.CLIENT_GAME_PORT_LOWER;
import static gameInterfaces.serverGameInterfaces.ClientCallbackInterface.CLIENT_GAME_PORT_UPPER;
import static gameInterfaces.util.Util.rescaleGetBytes;

public class LobbyController implements Initializable {

    // =================================================================================================================
    // fields ==========================================================================================================

    private ClientLobbyInterface clientLobbyInterface;
    private String username;

    private Stage stage;
    private String token;
    private Timer timer;

    // =================================================================================================================
    // init ============================================================================================================

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    /**
     * Method to get an instance of the lobby controller. This also couples the connection to the lobby to the
     * client's lobby controller.
     *
     * @param primaryStage      new FXML stage
     * @param lobbyInterface    connection to the lobby
     * @return                  a new instance of the {@link LobbyController}
     */
    public static LobbyController initStage(Stage primaryStage, ClientLobbyInterface lobbyInterface) {

        LobbyController controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(
                    LobbyController.class.getResource("../resources/views/lobby_view.fxml")
            );
            Scene scene = new Scene(loader.load());
            controller = loader.getController();
            controller.clientLobbyInterface = lobbyInterface;
            scene.getStylesheets().add("resources/styles/clientStyle.css");
            primaryStage.setScene(scene);
            primaryStage.setTitle("Memory! - lobby");
            primaryStage.sizeToScene();
            primaryStage.setResizable(false);
            controller.stage = primaryStage;
        } catch (IOException e){
            e.printStackTrace();
        }
        return controller;
    }

    // =================================================================================================================
    // getters & setters ===============================================================================================

    public void setToken(String token) {
        this.token = token;
    }

    // =================================================================================================================
    // FXML and othre related methods ==================================================================================

    /**
     * Method to call in order to show the lobby stage and load the elements of the lobby. {@link #update()} will be
     * called to update the contents of the lobby and the {@link Timer} to refresh the lobby on a cycle is
     * instantiated.
     */
    public void show() {
        stage.show();
        try {
            username = clientLobbyInterface.getUsername(token);
            ((Label) stage.getScene().lookup("#username")).setText(username);
            update();
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    update();
                }
            }, 0, 10000);
        } catch (RemoteException e) {
            memoryLogger.info("broker might not be running.\n");
            e.printStackTrace();
        } catch (InvalidTokenException e) {
            memoryLogger.info(String.format("token %s no longer valid.\n", token));
            StartController.initStage(stage);
            e.printStackTrace();
        }
    }

    /**
     * Button method to refresh the lobby list.
     *
     * @param mouseEvent    default FXML parameter for {@link javafx.scene.layout.HBox} click events
     */
    @FXML public void refresh(MouseEvent mouseEvent) {
        update();
    }

    /**
     * Button method to log out.
     *
     * @param mouseEvent    default FXML parameter for {@link javafx.scene.layout.HBox} click events
     */
    @FXML public void logout(MouseEvent mouseEvent) {

        try {
            ((BrokerEntryInterface) (LocateRegistry.getRegistry(BROKER_HOST, BROKER_PORT))
                    .lookup(BROKER_NAME))
                    .enter()
                    .logout(token);
        } catch (RemoteException e) {
            memoryLogger.info("problem logging out...\n");
            e.printStackTrace();
        } catch (NotBoundException e) {
            memoryLogger.info("problem connecting to broker...\n");
            e.printStackTrace();
        }
        if (timer != null) timer.cancel();
        Platform.exit();
    }

    /**
     * Method to handle the updating of the lobby list. This method is used by {@link #timer} to refresh the lobby
     * on a cycle.
     */
    private void update() {
        try {
            memoryLogger.info("looking for new games...");

            // acquire new running games from lobby --------------------------------------------------------------------
            LinkedList<ClientGameInterface> games =
                    (LinkedList<ClientGameInterface>) clientLobbyInterface.getRunningGames(token);

            // update lobby list using Platform.runLater() in order to run it on the FXML Thread -----------------------
            VBox lobbyList = (VBox) stage.getScene().lookup("#lobby_list");
            Platform.runLater(() -> {lobbyList.getChildren().removeAll(lobbyList.getChildren());});
            for(ClientGameInterface gameInterface: games){
                Platform.runLater(() -> {
                    try {
                        lobbyList.getChildren().add(
                                LobbyListItemController.init(
                                        gameInterface.getGameInfo(),
                                        new SuccessCallbackInterface() {
                                                @Override
                                                public void success() {
                                                    join(gameInterface);
                                                }

                                                @Override
                                                public void failed() {
                                        spectate(gameInterface);
                                    }
                                        })
                        );
                    } catch (RemoteException e) {
                        // TODO reconnect ******************************************************************************
                        memoryLogger.info("problem getting game info.");
                        e.printStackTrace();
                    }
                });
            }
        } catch (RemoteException e) {
            memoryLogger.severe("connected application server might be down");
            e.printStackTrace();
        } catch (InvalidTokenException e) {
            memoryLogger.info("token is no longer valid");
            e.printStackTrace();
        }
    }

    /**
     * Method to start spectating a game. A new instance of {@link GameController} is generated and a
     * {@link ClientGameInterface} is coupled to it.
     *
     * @param gameInterface     the connection to a game
     */
    private void spectate(ClientGameInterface gameInterface) {
        GameController gameController = initGameController(gameInterface, null, token, username);
        gameController.spectate();
        gameController.show();
    }

    /**
     * Method to join a game. A new instance of {@link GameController} is generated and a {@link ClientGameInterface}
     * is coupled to it
     *
     * @param gameInterface     the connection to the game to join
     */
    private void join(ClientGameInterface gameInterface) {
        GameController gameController = initGameController(gameInterface, null, token, username);
        gameController.join();
    }

    /**
     * Button method to create a new game. A new instance of the {@link GameCreatorController} is generated. A
     * {@link SuccessCallbackInterface} is coupled to it in order to pass the request to create a game to the
     * lobby connection.
     *
     * @param mouseEvent    default {@link javafx.scene.layout.HBox} click parameter
     */
    @FXML public void createGame(MouseEvent mouseEvent) {
        memoryLogger.info("starting process to make new game.\n");
        GameCreatorController creatorController = GameCreatorController.initStage();
        creatorController.setCallbackInterface(new SuccessCallbackInterface() {
            @Override
            public void success() {
                try {
                    memoryLogger.info("creating game...\n");
                    HostGameInterface gameInterface = clientLobbyInterface.createGame(
                            token,
                            creatorController.getName(),
                            creatorController.getWidth(),
                            creatorController.getHeight(),
                            creatorController.getLimit()
                    );

                    gameInterface.setFaces(token, creatorController.getFacesBytes());
                    gameInterface.setFacesStandard(token, new ArrayList<>(creatorController.getStandardFaces()));

                    if(creatorController.getBack() != null) {
                        gameInterface.setBack(
                                token,
                                rescaleGetBytes(creatorController.getBack().getPath())
                        );
                    } else {
                        gameInterface.setBackStandard(token);
                    }
                    GameController controller = initGameController(gameInterface, gameInterface, token, username);
                    controller.open();

                    memoryLogger.info("game created.\n");
                } catch (RemoteException e) {
                    memoryLogger.severe("broker might not be available.\n");
                    e.printStackTrace();
                } catch (InvalidTokenException e) {
                    memoryLogger.info("token no longer valid.\n");
                    e.printStackTrace();
                    StartController.initStage(stage);
                } catch (IOException e) {
                    memoryLogger.severe("problem reading card image.\n");
                    e.printStackTrace();
                }
            }

            @Override
            public void failed() {
                memoryLogger.info("failed to create game.\n");
            }
        });
        creatorController.show();
    }

    /**
     * Method to start a new {@link Stage} for a game with a specified {@link GameController}. This method will
     * attempt to open a new {@link ClientCallbackInterface} on an available port and pass
     * it to a {@link GameController}.
     *
     * @param gameInterface     normal connection to a specified game
     * @param hostGameInterface host connection to a specific game
     * @param token             user's unique token
     * @param username          user's username
     * @return                  new instance of a {@link GameController}
     */
    private static GameController initGameController(
            ClientGameInterface gameInterface,
            HostGameInterface hostGameInterface,
            String token,
            String username){

        GameController gameController = GameController.initStage();
        for(int port = CLIENT_GAME_PORT_LOWER; port < CLIENT_GAME_PORT_UPPER; port++){
            memoryLogger.info(String.format("attempting to open interface on port %d", port));
            try{
                Registry registry = LocateRegistry.createRegistry(port);
                registry.rebind(CLIENT_GAME_NAME, gameController.getClientCallbackInterface());
                gameController.setGameInterface(gameInterface);
                gameController.setHostGameInterface(hostGameInterface);
                gameController.setToken(token);
                gameController.setUsername(username);
                memoryLogger.info(String.format("interface opened on port %d!", port));
                break;
            } catch (RemoteException e) {
                memoryLogger.info(String.format("port %d already taken", port));
                e.printStackTrace();
            }
        }
        return gameController;
    }
}
