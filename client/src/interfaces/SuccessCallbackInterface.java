package interfaces;

public interface SuccessCallbackInterface {
    void success();
    void failed();
}
