package application;

import controllers.StartController;
import javafx.application.Application;
import javafx.stage.Stage;

import java.util.logging.Logger;

public class StartApplication extends Application{

    public static Logger memoryLogger = Logger.getLogger("Memory!");

    @Override
    public void start(Stage primaryStage) throws Exception {

        StartController.initStage(primaryStage);
    }

    @Override
    public void stop() throws Exception {

        memoryLogger.info("closing...\n");
        System.exit(0);
        super.stop();
    }

    public static void main(String[] args){
        launch(args);
    }
}
