package gameInterfaces.clientGameInterfaces;

import exceptions.InvalidTokenException;
import exceptions.NotEnoughCardsException;
import gameInterfaces.serverGameInterfaces.ClientCallbackInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.UUID;

public interface HostGameInterface extends Remote, ClientGameInterface{

    void setFaces(String token, ArrayList<byte[]> faces) throws RemoteException;
    void setFacesStandard(String token, ArrayList<UUID> faces) throws RemoteException;
    void setBack(String token, byte[] back) throws RemoteException;
    void setBackStandard(String token) throws RemoteException;
    void open(String token, String username, ClientCallbackInterface callbackInterface) throws RemoteException, NotEnoughCardsException, NotEnoughCardsException, InvalidTokenException;
}
