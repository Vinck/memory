package gameInterfaces.clientGameInterfaces;

import gameInterfaces.dataClasses.GameInfo;
import exceptions.*;
import gameInterfaces.serverGameInterfaces.ClientCallbackInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;

public interface ClientGameInterface extends Remote {

    String GAME_CONTROL_HOST = "localhost";
    int GAME_CONTROL_PORT_LOWER = 10000;
    int GAME_CONTROL_PORT_UPPER = 10499;
    String GAME_CONTROL_NAME = "memory";

    GameInfo getGameInfo() throws RemoteException;
    void join(String token, String username, ClientCallbackInterface callbackInterface) throws RemoteException, InvalidTokenException, FullGameException, GameNotOpenException;
    void spectate(String token, String username, ClientCallbackInterface clientCallbackInterface) throws RemoteException;
    void logout(String token, ClientCallbackInterface clientCallbackInterface) throws RemoteException;
    void selectCard(String token, int number) throws RemoteException, NotYourTurnException, NotenoughPlayersException;
    void sendMessage(String token, String message) throws RemoteException;
    void requestCards(String token) throws RemoteException;
    void requestBacks(String token) throws RemoteException;
    HashMap<String, Integer> requestPlayers(String token) throws RemoteException;
}
