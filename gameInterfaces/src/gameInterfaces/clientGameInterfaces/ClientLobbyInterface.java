package gameInterfaces.clientGameInterfaces;

import exceptions.InvalidTokenException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ClientLobbyInterface extends Remote{

    // =================================================================================================================
    // static values ===================================================================================================
    String LOBBY_HOST = "localhost";
    int LOBBY_PORT_LOWER = 13000;
    int LOBBY_PORT_UPPER = 13499;
    String LOBBY_NAME = "lobby";

    // =================================================================================================================
    // methods =========================================================================================================

    /**
     * Method to acquire the username given a certain token.
     *
     * @param token user's unique token
     * @return      the user's username
     * @throws RemoteException          default rmi exception
     * @throws InvalidTokenException    thrown when the database detects an invalid token
     */
    String getUsername(String token) throws RemoteException, InvalidTokenException;

    /**
     * Method for clients to call in order to get a list of all running games. This list will be user to construct
     * the lobby on the client side.
     *
     * @param token a user's unique token
     * @return      a list of game interfaces
     * @throws RemoteException          default rmi exception
     * @throws InvalidTokenException    thrown when the database detects an invalid token
     */
    List<ClientGameInterface> getRunningGames(String token) throws RemoteException, InvalidTokenException;

    /**
     * Method for client to call to create a new game with the given specifications. This will return a
     * {@link HostGameInterface} through which the cards can be set.
     *
     * @param token     user's unique token
     * @param name      name of the game
     * @param width     width of the game field
     * @param height    height of the game field
     * @param limit     player limit for the game
     * @return          connection to the game's control functions
     * @throws RemoteException          default rmi exception
     * @throws InvalidTokenException    thrown when the database detects an invalid token
     */
    HostGameInterface createGame(String token, String name, int width, int height, int limit)
            throws RemoteException, InvalidTokenException;
}