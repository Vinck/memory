package gameInterfaces.dataClasses;

import gameInterfaces.serverGameInterfaces.ClientCallbackInterface;

import java.io.Serializable;
import java.util.*;

public class Game implements Serializable{

    private UUID gameId;
    private UUID host;
    private String hostName;

    private String name;
    private int width;
    private int height;
    private int limit;

    private byte[] back;
    private ArrayList<byte[]> faces;
    private ArrayList<Card> cards;
    private int[] solution;
    private ArrayList<UUID> standardFaces;

    private UUID playing;
    private HashMap<UUID, ClientCallbackInterface> players;
    private LinkedList<UUID> playerOrder;
    private HashMap<UUID, String> playerNames;
    private HashMap<String, Integer> scores;
    private List<Card> currentlyFlipped;

    private boolean open = false;

    public Game(UUID host, String hostName, int width, int height, int limit, String name) {
        this.host = host;
        this.hostName = hostName;
        this.name = name;
        this.width = width;
        this.height = height;
        this.limit = limit;

        this.playerNames = new HashMap<>();
        this.players = new HashMap<>();
        this.scores = new HashMap<>();

        this.cards = new ArrayList<>(width * height);
        for (int i = 0; i < width * height; i++) cards.add(new Card(i, false));
        this.solution = new int[width*height];
        this.playerOrder = new LinkedList<>();
        this.currentlyFlipped = new LinkedList<>();
    }

    public UUID getGameId() {
        return gameId;
    }

    public void setGameId(UUID gameId) {
        this.gameId = gameId;
    }

    public UUID getHost() {
        return host;
    }

    public void setHost(UUID host) {
        this.host = host;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public byte[] getBack() {
        return back;
    }

    public void setBack(byte[] back) {
        this.back = back;
    }

    public ArrayList<byte[]> getFaces() {
        return faces;
    }

    public void setFaces(ArrayList<byte[]> faces) {
        this.faces = faces;
    }

    public ArrayList<UUID> getStandardFaces() {
        return standardFaces;
    }

    public void setStandardFaces(ArrayList<UUID> standardFaces) {
        this.standardFaces = standardFaces;
    }

    public UUID getPlaying() {
        return playing;
    }

    public void setPlaying(UUID playing) {
        this.playing = playing;
    }

    public HashMap<UUID, ClientCallbackInterface> getPlayers() {
        return players;
    }

    public void setPlayers(HashMap<UUID, ClientCallbackInterface> players) {
        this.players = players;
    }

    public HashMap<UUID, String> getPlayerNames() {
        return playerNames;
    }

    public void setPlayerNames(HashMap<UUID, String> playerNames) {
        this.playerNames = playerNames;
    }

    public HashMap<String, Integer> getScores() {
        return scores;
    }

    public void setScores(HashMap<String, Integer> scores) {
        this.scores = scores;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public void setCards(ArrayList<Card> cards) {
        this.cards = cards;
    }

    public List<Card> getCurrentlyFlipped() {
        return currentlyFlipped;
    }

    public void setCurrentlyFlipped(List<Card> currentlyFlipped) {
        this.currentlyFlipped = currentlyFlipped;
    }

    public LinkedList<UUID> getPlayerOrder() {
        return playerOrder;
    }

    public void setPlayerOrder(LinkedList<UUID> playerOrder) {
        this.playerOrder = playerOrder;
    }

    public int[] getSolution() {
        return solution;
    }

    public void setSolution(int[] solution) {
        this.solution = solution;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public void generateField(){
        HashSet<Integer> temp = new HashSet<>();
        Random random = new Random();
        int face;
        for (int i = 0; i < solution.length; i++) {
            while (temp.contains(face = random.nextInt(solution.length))){}
            temp.add(face);
            solution[i] = face % faces.size();
        }
    }

    public int faceAt(int cardNumber) {
        return solution[cardNumber];
    }
}