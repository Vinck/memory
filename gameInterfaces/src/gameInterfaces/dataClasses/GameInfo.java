package gameInterfaces.dataClasses;

import java.io.Serializable;

public class GameInfo implements Serializable{

    private String name;
    private String host;
    private int limit;
    private int players;
    private int width;
    private int height;
    private boolean open;

    public GameInfo(String name, String host, int players, int width, int height, boolean open, int limit) {
        this.name = name;
        this.host = host;
        this.limit = limit;
        this.players = players;
        this.width = width;
        this.height = height;
        this.open = open;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getPlayers() {
        return players;
    }

    public void setPlayers(int players) {
        this.players = players;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }
}
