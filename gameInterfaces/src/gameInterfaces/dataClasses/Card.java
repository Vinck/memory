package gameInterfaces.dataClasses;

import java.io.Serializable;

public class Card implements Serializable{

    public static int WIDTH = 89;
    public static int HEIGHT = 125;

    public static int GAME_WIDTH = 119;
    public static int GAME_HEIGHT = 167;

    private int identifier;
    private byte[] face;
    private boolean flipped;

    public Card(int identifier, boolean flipped) {
        this.identifier = identifier;
        this.flipped = flipped;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public byte[] getFace() {
        return face;
    }

    public void setFace(byte[] face) {
        this.face = face;
    }

    public boolean isFlipped() {
        return flipped;
    }

    public void setFlipped(boolean flipped) {
        this.flipped = flipped;
    }
}
