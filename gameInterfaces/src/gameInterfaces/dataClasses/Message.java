package gameInterfaces.dataClasses;

import java.io.Serializable;
import java.sql.Timestamp;

public class Message implements Serializable{

    private String username;
    private String message;
    private Timestamp time;

    public Message(String username, String message, Timestamp time) {
        this.username = username;
        this.message = message;
        this.time = time;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
