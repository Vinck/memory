package gameInterfaces.databaseGameInterfaces;

import gameInterfaces.dataClasses.Game;
import exceptions.CardNotFoundException;
import exceptions.GameNotFoundException;
import exceptions.InvalidTokenException;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public interface DatabaseGameInterface extends Remote{

    // =================================================================================================================
    // static fields ===================================================================================================
    String DATABASE_GAME_HOST = "localhost";
    int DATABASE_GAME_PORT_LOWER = 12500;
    int DATABASE_GAME_PORT_UPPER = 12999;
    String DATABASE_GAME_NAME = "database_game";

    // =================================================================================================================
    // methods =========================================================================================================
    /**
     * Method to check the validity of a token. A token is invalid if it either does not exist in the database ot
     * if it has expired, it expires after 24 hours.
     *
     * @param token certain unique token
     *
     * @return      true if valid
     * @throws RemoteException          default rmi exception
     * @throws InvalidTokenException    thrown when the token is no longer valid
     */
    boolean checkToken(String token) throws RemoteException, InvalidTokenException;

    /**
     * Method to acquire username coupled to a certain unique token.
     *
     * @param token certain unique token
     *
     * @return      the coupled username
     * @throws RemoteException          default rmi exception
     * @throws InvalidTokenException    thrown when the token is no longer valid, checked with
     *                                  {@link DatabaseGameInterface#checkToken(String)}
     */
    String getUsername(String token) throws RemoteException, InvalidTokenException;

    /**
     * Method to acquire a list of all running games persisted to the database. The games are returned under the form
     * of a {@link Game} object in a list. This list can later be used to restore games when a server went down.
     *
     * @return  list of games in database
     * @throws RemoteException  default rmi exception
     */
    List<Game> getRunningGames() throws RemoteException;

    /**
     * Method to query the database for a certain stored card fae or back using a unique card id.
     *
     * @param cardId    stored card id
     *
     * @return          the stored image byte array
     * @throws RemoteException          default rmi exception
     * @throws CardNotFoundException    thrown when no card can be found for a certain card id
     */
    byte[] getCard(UUID cardId) throws RemoteException, CardNotFoundException;

    /**
     * Method to persist a new card face or back to the database. A new id will be assigned to the card image before
     * persisting.
     *
     * @param face  the face or back image as byte array
     *
     * @return      the id ssigned to the card
     * @throws RemoteException  default rmi exception
     */
    UUID uploadCard(byte[] face) throws RemoteException;

    /**
     * Method to persist the initial state of a game to the database, this method will assign a new unique id to
     * the game and return it to store it on the application server. The user attempting to start a new game will need
     * to have a valid identification token, the token will need to be checked before persisting the game. Card faces
     * and back are provided as card ids, information about the game are provided in the form of a
     * {@link Game} object. The facesIds array is used in combination with the {@link Game#cards} and
     * {@link Game#solution} in to generate a json representation to reconstruct the entire field, including the
     * cards already turned around.
     *
     * @param token     unique token of the user trying to open the game
     * @param facesIds  list of card ids for the game's faces
     * @param backId    card id of the card back used in the game
     * @param game      other game info
     *
     * @return          new unique game id
     * @throws RemoteException          default rmi exception
     * @throws InvalidTokenException    thrown when the hosts token is no longer valid
     */
    UUID persistGame(String token, ArrayList<UUID> facesIds, UUID backId, Game game)
            throws RemoteException, InvalidTokenException;

    /**
     * Method to update game info of an already persisted game. Game info should be updated each time a card pair is
     * discovered and the score is updated to match. Unnecessary updating of game should be avoided to prevent database
     * overload. The game info is passed as a {@link Game} object. The facesIds array is used in combination with the {@link Game#cards} and
     * {@link Game#solution} in to generate a json representation to reconstruct the entire field, including the
     * cards already turned around.
     *
     * @param gameId    unique id of the game
     * @param game      new game info
     * @param facesIds  list of card ids to find card image in database
     *
     * @throws RemoteException          default rmi exception
     * @throws GameNotFoundException    thrown when the database cannot find the specified game
     */
    void updateGame(String gameId, Game game, ArrayList<UUID> facesIds) throws RemoteException, GameNotFoundException;

    /**
     * Method to call to close a game. This will remove all data related to the game from the database and update the
     * global score. All none standard cards will also be erased from the database in order to keep the database clean.
     *
     * @param gameId    unique id of the game to be closed
     * @throws RemoteException  default rmi exception
     */
    void close(UUID gameId) throws RemoteException;
}
