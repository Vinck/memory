package gameInterfaces.serverGameInterfaces;

import gameInterfaces.dataClasses.Card;
import gameInterfaces.dataClasses.Message;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public interface ClientCallbackInterface extends Remote{

    String CLIENT_GAME_HOST = "localhost";
    int CLIENT_GAME_PORT_LOWER = 11500;
    int CLIENT_GAME_PORT_UPPER = 11999;
    String CLIENT_GAME_NAME = "memory_client";

    void kick() throws RemoteException;
    void pushCards(ArrayList<byte[]> cards) throws RemoteException;
    void pushCardBack(byte[] cardBack) throws RemoteException;
    void receiveMessage(Message message) throws RemoteException;
    void pushField(ArrayList<Card> cards, int width, int height) throws RemoteException;
    void flipCard(int card, int face) throws RemoteException;
    void correct(int first, int firstFace, int second, int secondFace) throws RemoteException;
    void incorrect(int first, int second) throws RemoteException;
    void play() throws RemoteException;
    void stopPlaying() throws RemoteException;
    void updateScoreList(String username, int score) throws RemoteException;
    void updatePlaying(String username) throws RemoteException;
    void winner(List<String> winners, int score) throws RemoteException;
    void updateSpectators(String username, boolean spectating)throws RemoteException;
}
