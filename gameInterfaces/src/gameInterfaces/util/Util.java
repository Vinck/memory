package gameInterfaces.util;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import static gameInterfaces.dataClasses.Card.HEIGHT;
import static gameInterfaces.dataClasses.Card.WIDTH;
import static java.awt.RenderingHints.KEY_INTERPOLATION;
import static java.awt.RenderingHints.VALUE_INTERPOLATION_BILINEAR;

public class Util {
    /**
     * Method to read in a card image from a file, rescale it and return it in the form of a byte array. Any time
     * a card is read from a file to be inserted into the database this function should be called in order to keep the
     * dimensions of the cards consistent, namely 89x125.
     *
     * @param uri   the URI of the card image file
     *
     * @return      the byte array representation of the card
     * @throws IOException  default io exception
     */
    public static byte[] rescaleGetBytes(String uri) throws IOException {

        byte[] resizedArray;

        // read image from file ----------------------------------------------------------------------------------------
        BufferedImage original = ImageIO.read(new File(uri));
        BufferedImage resized = new BufferedImage(WIDTH, HEIGHT, original.getType());

        // resize and rescale ------------------------------------------------------------------------------------------
        Graphics2D graphics2D = resized.createGraphics();
        graphics2D.setRenderingHint(KEY_INTERPOLATION, VALUE_INTERPOLATION_BILINEAR);
        graphics2D.drawImage(
                original,
                0,
                0,
                WIDTH,
                HEIGHT,
                0,
                0,
                original.getWidth(),
                original.getHeight(),
                null
        );
        graphics2D.dispose();

        // output image data to byte array -----------------------------------------------------------------------------
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ImageIO.write(resized, "png", byteArrayOutputStream);
        byteArrayOutputStream.flush();
        resizedArray = byteArrayOutputStream.toByteArray();
        byteArrayOutputStream.close();

        return resizedArray;
    }
}
