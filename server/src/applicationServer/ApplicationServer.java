package applicationServer;

import applicationServer.applicationRMI.ApplicationServerControl;
import applicationServer.lobbyRMI.AccountControl;
import applicationServer.lobbyRMI.LobbyControl;
import applicationServer.network.ServerNetworkManager;
import gameInterfaces.clientGameInterfaces.ClientGameInterface;
import gameInterfaces.clientGameInterfaces.ClientLobbyInterface;
import dataClasses.Server;
import databaseAccountInterfaces.DatabaseAccountInterface;
import gameInterfaces.databaseGameInterfaces.DatabaseGameInterface;
import gameServerEntry.GameServerEntryInterface;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

import static applicationServerControlInterfaces.ApplicationServerControlInterface.*;
import static clientAccountInterfaces.ClientAccountInterface.*;
import static gameInterfaces.clientGameInterfaces.ClientLobbyInterface.LOBBY_NAME;
import static gameInterfaces.clientGameInterfaces.ClientLobbyInterface.LOBBY_PORT_LOWER;
import static gameInterfaces.clientGameInterfaces.ClientLobbyInterface.LOBBY_PORT_UPPER;
import static databaseAccountInterfaces.DatabaseAccountInterface.DATABASE_ACCOUNTS_NAME;
import static gameServerEntry.GameServerEntryInterface.GAME_ENTRY_HOST;
import static gameServerEntry.GameServerEntryInterface.GAME_ENTRY_NAME;
import static gameServerEntry.GameServerEntryInterface.GAME_ENTRY_PORT;

public class ApplicationServer {

    public static Logger applicationServerLogger = Logger.getLogger("application server");

    private void runServer(){

        // temporary variables -----------------------------------------------------------------------------------------
        int applicationServerControlPort = -1;
        int accountControlPort = -1;
        // temporary variables -----------------------------------------------------------------------------------------

        // connect to broker -------------------------------------------------------------------------------------------
        GameServerEntryInterface gameServerEntryInterface = null;
        try {
            Registry entryRegistry = LocateRegistry.getRegistry(GAME_ENTRY_HOST, GAME_ENTRY_PORT);
            gameServerEntryInterface =
                    (GameServerEntryInterface) entryRegistry.lookup(GAME_ENTRY_NAME);
        } catch (RemoteException e) {
            applicationServerLogger.info("could not connect to broker's game entry");
            e.printStackTrace();
        } catch (NotBoundException e) {
            applicationServerLogger.info(String.format(
                    "broker's game entry might not be bound to %s:%d",
                    GAME_ENTRY_HOST,
                    GAME_ENTRY_PORT
            ));
            e.printStackTrace();
        }

        // stop if not connected ---------------------------------------------------------------------------------------
        if (gameServerEntryInterface == null) return;

        // generate application server manager object ------------------------------------------------------------------
        ServerNetworkManager serverNetworkManager = new ServerNetworkManager(gameServerEntryInterface);

        // generate list for running games -----------------------------------------------------------------------------
        LinkedList<ClientGameInterface> runningGames = new LinkedList<>();

        applicationServerLogger.info("attempting to start application server...\n");

        // start application server control ----------------------------------------------------------------------------
        applicationServerLogger.info("attemptingstart application server control...");
        for (int port = APPLICATION_CONTROL_PORT_LOWER; port < APPLICATION_CONTROL_PORT_UPPER; port++) {
            applicationServerLogger.info(String.format("attempting to use port %d", port));
            try{
                Registry registry = LocateRegistry.createRegistry(port);
                registry.rebind(APPLICATION_CONTROL_NAME, new ApplicationServerControl(serverNetworkManager));
                applicationServerControlPort = port;
                applicationServerLogger.info(
                        String.format("succeeded! application server control using port %d", port)
                );
                break;
            } catch (RemoteException e) {
                applicationServerLogger.severe(String.format("failed to use port %d", port));
                e.printStackTrace();
            }
        }

        // start account control ---------------------------------------------------------------------------------------
        applicationServerLogger.info("attempting to start account control...");
        for (int port = ACCOUNTS_PORT_LOWER; port < ACCOUNTS_PORT_UPPER; port++) {
            applicationServerLogger.info(String.format("attempting to use port %d", port));
            try{
                Registry registry = LocateRegistry.createRegistry(port);
                registry.rebind(ACCOUNTS_NAME, new AccountControl(serverNetworkManager));
                accountControlPort = port;
                applicationServerLogger.info(
                        String.format("succeeded! account control using port %d", port)
                );
                break;
            } catch (RemoteException e) {
                applicationServerLogger.severe(String.format("failed to use port %d", port));
                e.printStackTrace();
            }
        }

        // register with broker ----------------------------------------------------------------------------------------
        if (applicationServerControlPort != -1 && accountControlPort != -1) {
            brokerRegister(
                    gameServerEntryInterface,
                    serverNetworkManager,
                    applicationServerControlPort,
                    accountControlPort,
                    runningGames
            );
            applicationServerLogger.info("server started successfully!");
        } else {
            applicationServerLogger.info("problem while starting server...");
        }
    }

    /**
     * Method to register the application control and the account control with the broker.
     *
     * @param gameServerEntryInterface  connection to the broker to register the application server
     * @param serverNetworkManager      local application server network manager
     * @param applicationControlPort    port number of application server control
     * @param accountControlPort        port number of account control
     */
    private void brokerRegister(
            GameServerEntryInterface gameServerEntryInterface,
            ServerNetworkManager serverNetworkManager,
            int applicationControlPort,
            int accountControlPort,
            LinkedList<ClientGameInterface> runningGames) {
        try {
            applicationServerLogger.info("attempting to register with broker...\n");
            serverNetworkManager.setRunningServers(
                    gameServerEntryInterface.enterApplicationServerControl(
                            APPLICATION_CONTROL_HOST,
                            applicationControlPort
                    )
            );
            serverNetworkManager.connectAllRunningServers();
            startLobby(
                    serverNetworkManager,
                    gameServerEntryInterface,
                    accountControlPort
            );
            applicationServerLogger.info("registered with broker!\n");
        } catch (RemoteException e) {
            applicationServerLogger.severe("broker registering service might not be running.\n");
            e.printStackTrace();
        }
    }

    /**
     * Method to start lobby controller and connect a database server.
     *
     * @param serverNetworkManager      local server network manager
     * @param gameServerEntryInterface  connection to the broker
     * @param accountControlPort        port the account interface was opened on
     */
    private void startLobby(
            ServerNetworkManager serverNetworkManager,
            GameServerEntryInterface gameServerEntryInterface,
            int accountControlPort) {

        // timer task to retry when connection to database fails -------------------------------------------------------
        TimerTask retryTask = new TimerTask() {
            @Override
            public void run() {
                startLobby(serverNetworkManager, gameServerEntryInterface, accountControlPort);
            }
        };

        // attempt retrieval of database server info -------------------------------------------------------------------
        Server server;
        try {
            server = gameServerEntryInterface.enterAccountControl(ACCOUNTS_HOST, accountControlPort);
        } catch (RemoteException e) {
            applicationServerLogger.info("problem connecting to broker, retrying in a bit...");
            e.printStackTrace();
            new Timer().schedule(retryTask, 5000);
            // interrupt rest of execution -----------------------------------------------------------------------------
            return;
        }

        // attempt connection only if information is actually received -------------------------------------------------
        if (server != null) {
            try {
                Registry registry = LocateRegistry.getRegistry(server.getHost(), server.getPort());
                DatabaseAccountInterface databaseAccountInterface =
                        (DatabaseAccountInterface) registry.lookup(DATABASE_ACCOUNTS_NAME);
                serverNetworkManager.setDatabaseAccountInterface(databaseAccountInterface);
                serverNetworkManager.setDatabaseGameInterface(
                        databaseAccountInterface.getDatabaseGameInterface()
                );
                applicationServerLogger.info(String.format(
                        "successfully connected to database accounts interface at %s:%d\nstarting lobby now...",
                        server.getHost(),
                        server.getPort()
                ));
                openLobby(serverNetworkManager, databaseAccountInterface.getDatabaseGameInterface());
            } catch (RemoteException e) {
                applicationServerLogger.info(String.format(
                        "problem connecting to database at %s:%d, retrying in a bit...",
                        server.getHost(),
                        server.getPort()
                ));
                e.printStackTrace();
                new Timer().schedule(retryTask, 5000);
            } catch (NotBoundException e) {
                applicationServerLogger.info(String.format(
                        "no database bound at at %s:%d, retrying in a bit...",
                        server.getHost(),
                        server.getPort()
                ));
                e.printStackTrace();
                new Timer().schedule(retryTask, 5000);
            }
        }
        // retry after 5 seconds if no data received -------------------------------------------------------------------
        else {
            applicationServerLogger.info("could not receive database interfaces, retrying in a bit...");
            new Timer().schedule(retryTask, 5000);
        }
    }

    /**
     * Method to open lobby after the database has been connected successfully. The lobby is opened in the form of a
     * new {@link ClientLobbyInterface} running on a certain port between
     * {@link ClientLobbyInterface#LOBBY_PORT_LOWER} and
     * {@link ClientLobbyInterface#LOBBY_PORT_UPPER}. The lobby will also attempt to query all
     * running application servers to get their current running games.
     *
     * @param serverNetworkManager  the local server network manager object
     */
    private void openLobby(
            ServerNetworkManager serverNetworkManager,
            DatabaseGameInterface databaseGameInterface) {

        // check for an available port ---------------------------------------------------------------------------------
        Registry registry;
        for (int port = LOBBY_PORT_LOWER; port <= LOBBY_PORT_UPPER; port++) {
            applicationServerLogger.info(String.format(
                    "attempting to open lobby on port %d...",
                    port
            ));
            try {
                registry = LocateRegistry.createRegistry(port);
                LobbyControl lobbyControl = new LobbyControl(serverNetworkManager, databaseGameInterface);
                registry.rebind(LOBBY_NAME, lobbyControl);
                serverNetworkManager.setLobbyInterface(lobbyControl);
                applicationServerLogger.info(String.format(
                        "lobby opened on port %d!",
                        port
                ));
                break;
            } catch (RemoteException e) {
                applicationServerLogger.info(String.format(
                        "failed to open on port %d!",
                        port
                ));
                e.printStackTrace();
            }
            if (port == LOBBY_PORT_UPPER) {
                applicationServerLogger.severe(String.format(
                        "completely failed to open lobby...\nno free port between %d and %d",
                        LOBBY_PORT_LOWER,
                        LOBBY_PORT_UPPER
                ));
            }
        }
    }

    // ================================================================================================================
    // server start ===================================================================================================

    public static void main(String[] args){
        new ApplicationServer().runServer();
    }
}
