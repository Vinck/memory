package applicationServer.applicationRMI;

import gameInterfaces.clientGameInterfaces.HostGameInterface;
import gameInterfaces.dataClasses.Card;
import gameInterfaces.dataClasses.Game;
import gameInterfaces.dataClasses.GameInfo;
import gameInterfaces.dataClasses.Message;
import gameInterfaces.databaseGameInterfaces.DatabaseGameInterface;
import exceptions.*;
import interfaces.GameCloseInterface;
import gameInterfaces.serverGameInterfaces.ClientCallbackInterface;

import java.io.Serializable;
import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Timestamp;
import java.util.*;

import static applicationServer.ApplicationServer.applicationServerLogger;

public class GameControl extends UnicastRemoteObject implements HostGameInterface, Serializable{

    // =================================================================================================================
    // fields ==========================================================================================================

    private Game game;
    private UUID backId;
    private ArrayList<UUID> facesIds;
    private GameCloseInterface onClose;
    private DatabaseGameInterface databaseGameInterface;

    // =================================================================================================================
    // init ============================================================================================================

    public GameControl(
            UUID host,
            String name,
            int width,
            int height,
            int limit,
            DatabaseGameInterface databaseGameInterface) throws RemoteException, InvalidTokenException {
        init(host, name, width, height, limit, databaseGameInterface);
    }

    /**
     * Method used to initialise the perameters of the game and set the games {@link DatabaseGameInterface}.
     *
     * @param host                  the game's host current token
     * @param name                  the game's name
     * @param width                 the game field's width
     * @param height                the game field's height
     * @param limit                 the game's player limit
     * @param databaseGameInterface the connetcion to the database
     * @throws RemoteException          default rmi exception
     * @throws InvalidTokenException    thrown when the database detects the hosts token is invalid
     */
    private void init(
            UUID host,
            String name,
            int width,
            int height,
            int limit,
            DatabaseGameInterface databaseGameInterface) throws RemoteException, InvalidTokenException {
        this.game = new Game(host, databaseGameInterface.getUsername(host.toString()), width, height, limit, name);
        this.facesIds = new ArrayList<>(game.getWidth() * game.getHeight());
        this.databaseGameInterface = databaseGameInterface;
    }

    // =================================================================================================================
    // interface overrides =============================================================================================

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFaces(String token, ArrayList<byte[]> faces) throws RemoteException {
        if(game.getHost().toString().equals(token)){
            game.setFaces(faces);
            for(byte[] face: faces) facesIds.add(databaseGameInterface.uploadCard(face));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFacesStandard(String token, ArrayList<UUID> faces) throws RemoteException {
        if(game.getHost().toString().equals(token)){
            game.setStandardFaces(faces);
            for(UUID uuid: faces){
                try {
                    if(uuid != null) game.getFaces().add(databaseGameInterface.getCard(uuid));
                    facesIds.add(uuid);
                } catch (CardNotFoundException e) {
                    // shouldn't appear, database issues if appears.
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBack(String token, byte[] back) throws RemoteException {
        if(game.getHost().toString().equals(token)){
            backId = databaseGameInterface.uploadCard(back);
            game.setBack(back);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBackStandard(String token) throws RemoteException {
        if(game.getHost().toString().equals(token)){
            try {
                backId = UUID.fromString("00000000-0000-0000-0000-000000000000");
                game.setBack(databaseGameInterface.getCard(backId));
            } catch (CardNotFoundException e) {
                applicationServerLogger.info("fuck man, I don't know...");
                e.printStackTrace();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void open(String token, String username, ClientCallbackInterface callbackInterface)
            throws RemoteException, NotEnoughCardsException, InvalidTokenException {

        if(game.getFaces().size() + game.getStandardFaces().size() < (game.getWidth() * game.getHeight()) / 2){
            // shouldn't occur if game creation works
            throw new NotEnoughCardsException();
        } else {
            game.setHost(UUID.fromString(token));
            game.getPlayers().put(UUID.fromString(token), callbackInterface);
            game.getPlayerNames().put(UUID.fromString(token), username);
            game.getPlayerOrder().addLast(UUID.fromString(token));
            game.generateField();
            game.getScores().put(username, 0);
            game.setPlaying(game.getPlayerOrder().getFirst());
            callbackInterface.pushCardBack(game.getBack());
            callbackInterface.pushCards(game.getFaces());
            callbackInterface.pushField(game.getCards(), game.getWidth(), game.getHeight());
            updateScores(callbackInterface);
            callbackInterface.updatePlaying(null);
            game.setGameId(databaseGameInterface.persistGame(
                    token,
                    facesIds,
                    backId,
                    game
            ));
            game.setOpen(true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GameInfo getGameInfo() throws RemoteException {
        return new GameInfo(
                game.getName(),
                game.getHostName(),
                game.getPlayerOrder().size(),
                game.getWidth(),
                game.getHeight(),
                game.isOpen(),
                game.getLimit());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void join(String token, String username, ClientCallbackInterface callbackInterface)
            throws RemoteException, FullGameException, GameNotOpenException {

        // TODO updateGame onJoin **************************************************************************************

        if(game.isOpen()) {

            if(game.getLimit() > game.getPlayerOrder().size()) {
                game.getPlayers().put(UUID.fromString(token), callbackInterface);
                game.getPlayerNames().put(UUID.fromString(token), username);
                game.getPlayerOrder().addLast(UUID.fromString(token));
                game.getScores().put(username, 0);

                callbackInterface.pushCardBack(game.getBack());
                callbackInterface.pushCards(game.getFaces());
                callbackInterface.pushField(game.getCards(), game.getWidth(), game.getHeight());
                for (ClientCallbackInterface updateCallback : game.getPlayers().values()) {
                    updateScores(updateCallback);
                }
                if(game.getPlayerOrder().size() > 1){
                    for (UUID uuid: game.getPlayers().keySet()){
                        if (uuid != game.getPlaying()) {
                            game.getPlayers().get(uuid).updatePlaying(game.getPlayerNames().get(game.getPlaying()));
                        }
                    }
                    game.getPlayers().get(game.getPlaying()).play();
                } else {
                    for(ClientCallbackInterface updateCallback: game.getPlayers().values()){
                        updateCallback.updatePlaying(null);
                    }
                }
            } else{
                spectate(token, username, callbackInterface);
                throw new FullGameException();
            }
        } else throw new GameNotOpenException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void spectate(String token, String username, ClientCallbackInterface callbackInterface)
            throws RemoteException {

        game.getPlayers().put(UUID.fromString(token), callbackInterface);
        game.getPlayerNames().put(UUID.fromString(token), username);

        callbackInterface.pushCardBack(game.getBack());
        callbackInterface.pushCards(game.getFaces());
        callbackInterface.pushField(game.getCards(), game.getWidth(), game.getHeight());
        for (ClientCallbackInterface updateCallback : game.getPlayers().values()) {
            updateScores(updateCallback);
        }
    }

    /**
     * Method to push an update of the scoreboard and spectator list to a specific connected user.
     *
     * @param updateCallback    user to push update to
     * @throws RemoteException  default rmi exception
     */
    private void updateScores(ClientCallbackInterface updateCallback) throws RemoteException {
        for(UUID uuid: game.getPlayerOrder()){
            updateCallback.updateScoreList(
                    game.getPlayerNames().get(uuid),
                    game.getScores().get(game.getPlayerNames().get(uuid))
            );
        }
        for(UUID uuid: game.getPlayers().keySet()){
            if(!game.getPlayerOrder().contains(uuid)) {
                updateCallback.updateSpectators(game.getPlayerNames().get(uuid), true);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logout(String token, ClientCallbackInterface clientCallbackInterface) throws RemoteException {
        if(!game.getPlayerOrder().contains(UUID.fromString(token))){
            for(ClientCallbackInterface callbackInterface: game.getPlayers().values()){
                callbackInterface.updateSpectators(game.getPlayerNames().get(UUID.fromString(token)), false);
            }
        }
        if(game.getPlayers().keySet().contains(UUID.fromString(token))){
            game.getPlayers().remove(UUID.fromString(token));
            game.getPlayerOrder().remove(UUID.fromString(token));
        }
        if(game.getPlayers().keySet().isEmpty()) this.close();
        else if(game.getPlayerOrder().size() < 2){
            for(ClientCallbackInterface callbackInterface: game.getPlayers().values()){
                callbackInterface.updatePlaying(null);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectCard(String token, int cardNumber)
            throws RemoteException, NotYourTurnException, NotenoughPlayersException {
        // TODO add comments, cause it's kinda... complicated...sorry **************************************************
        if(game.getPlayers().size() < 2) throw new NotenoughPlayersException();
        if(game.getPlaying().toString().equals(token)){

            if(game.getCurrentlyFlipped().size() < 2 &&
                    (game.getCurrentlyFlipped().isEmpty() ||
                    cardNumber != game.getCards().indexOf(game.getCurrentlyFlipped().get(0)))){
                for(ClientCallbackInterface callbackInterface: game.getPlayers().values()){
                    callbackInterface.flipCard(cardNumber, game.faceAt(cardNumber));
                }
                game.getCurrentlyFlipped().add(game.getCards().get(cardNumber));
            }
            if(game.getCurrentlyFlipped().size() >= 2){

                int firstFace = game.faceAt(game.getCards().indexOf(game.getCurrentlyFlipped().get(0)));
                int secondFace = game.faceAt(game.getCards().indexOf(game.getCurrentlyFlipped().get(1)));
                if (firstFace == secondFace) {

                    game.getCurrentlyFlipped().get(0).setFlipped(true);
                    game.getCurrentlyFlipped().get(0).setFace(game.getFaces().get(firstFace));
                    game.getCurrentlyFlipped().get(1).setFlipped(true);
                    game.getCurrentlyFlipped().get(1).setFace(game.getFaces().get(secondFace));

                    game.getScores().put(
                            game.getPlayerNames().get(UUID.fromString(token)),
                            game.getScores().get(game.getPlayerNames().get(UUID.fromString(token))) + 1
                    );
                    for (ClientCallbackInterface callbackInterface : game.getPlayers().values()) {
                        callbackInterface.correct(
                                game.getCards().indexOf(game.getCurrentlyFlipped().get(0)),
                                firstFace,
                                game.getCards().indexOf(game.getCurrentlyFlipped().get(1)),
                                secondFace
                        );
                        callbackInterface.updateScoreList(
                                game.getPlayerNames().get(game.getPlaying()),
                                game.getScores().get(game.getPlayerNames().get(game.getPlaying()))
                        );
                    }

                    boolean done = true;
                    for(Card card: game.getCards()){
                        if (!card.isFlipped()){
                            done = false;
                            break;
                        }
                    }
                    if(done){
                        int winningScore = 0;
                        LinkedList<String> winners = new LinkedList<>();
                        for(String player: game.getScores().keySet()){
                            if (game.getScores().get(player) > winningScore){
                                winners.clear();
                                winningScore = game.getScores().get(player);
                                winners.add(player);
                            } else if(winningScore == game.getScores().get(player)){
                                winners.add(player);
                            }
                        }
                        for(ClientCallbackInterface clientCallbackInterface: game.getPlayers().values()){
                            clientCallbackInterface.winner(winners, winningScore);
                        }
                        game.setOpen(false);
                    } else {
                        game.getCurrentlyFlipped().clear();
                        /*
                        game.getPlayers().get(game.getPlaying()).play();
                        for(UUID uuid: game.getPlayers().keySet()){
                            if(uuid != game.getPlaying()){
                                game.getPlayers().get(uuid).updatePlaying(game.getPlayerNames().get(game.getPlaying()));
                            }
                        }
                        */
                    }
                    try {
                        databaseGameInterface.updateGame(game.getGameId().toString(), game, facesIds);
                    } catch (GameNotFoundException e) {
                        applicationServerLogger.severe(String.format(
                                "game doesn't exist for id %s",
                                game.getGameId().toString()
                        ));
                        e.printStackTrace();
                    }
                } else {
                    new Timer().schedule(new TimerTask() {
                        @Override
                        public void run() {
                            try {
                                for (ClientCallbackInterface callbackInterface : game.getPlayers().values()) {
                                    callbackInterface.incorrect(
                                            game.getCards().indexOf(game.getCurrentlyFlipped().get(0)),
                                            game.getCards().indexOf(game.getCurrentlyFlipped().get(1))
                                    );
                                }
                                game.setPlaying(game.getPlayerOrder().get((game.getPlayerOrder().indexOf(
                                        game.getPlaying()) + 1) % game.getPlayerOrder().size()
                                ));
                                game.getCurrentlyFlipped().clear();
                                for (UUID uuid: game.getPlayers().keySet()){
                                    if(uuid != game.getPlaying()){
                                        game.getPlayers()
                                                .get(uuid)
                                                .updatePlaying(game.getPlayerNames().get(game.getPlaying()));
                                    }
                                }
                                game.getPlayers().get(game.getPlaying()).play();
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }
                        }
                    }, 2000);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessage(String token, String message) throws RemoteException {
        for(UUID key: game.getPlayers().keySet()){
            if(!key.toString().equals(token)){
                try {
                    game.getPlayers().get(key).receiveMessage(new Message(
                            game.getPlayerNames().get(UUID.fromString(token)),
                            message,
                            new Timestamp(System.currentTimeMillis())
                    ));
                } catch (ConnectException e){
                    // ignore
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void requestCards(String token) throws RemoteException {
        game.getPlayers().get(UUID.fromString(token)).pushCards(game.getFaces());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void requestBacks(String token) throws RemoteException {
        game.getPlayers().get(UUID.fromString(token)).pushCardBack(game.getBack());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HashMap<String, Integer> requestPlayers(String token) throws RemoteException {
        return game.getScores();
    }

    // =================================================================================================================
    // game closing handler ============================================================================================

    /**
     * Method called when the game finishes to call both {@link DatabaseGameInterface#close(UUID)} and
     * {@link GameCloseInterface#close()}.
     *
     * @throws RemoteException  default rmi exception
     */
    private void close() throws RemoteException {
        databaseGameInterface.close(game.getGameId());
        onClose.close();
    }

    public void setOnClose(GameCloseInterface onClose){
        this.onClose = onClose;
    }
}
