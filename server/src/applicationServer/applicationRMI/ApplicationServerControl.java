package applicationServer.applicationRMI;

import applicationServer.network.ServerNetworkManager;
import applicationServerControlInterfaces.ApplicationServerControlInterface;
import gameInterfaces.clientGameInterfaces.ClientGameInterface;

import java.rmi.RemoteException;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;

public class ApplicationServerControl extends UnicastRemoteObject implements ApplicationServerControlInterface {

    // =================================================================================================================
    // fields ==========================================================================================================
    private ServerNetworkManager serverNetworkManager;

    // =================================================================================================================
    // init ============================================================================================================
    public ApplicationServerControl(ServerNetworkManager serverNetworkManager) throws RemoteException {
        this.serverNetworkManager = serverNetworkManager;
    }

    // =================================================================================================================
    // override methods ================================================================================================
    @Override
    public List<ClientGameInterface> getRunningGames() throws RemoteException {
        return serverNetworkManager.getLocalRunningGames();
    }
}
