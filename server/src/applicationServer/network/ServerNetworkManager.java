package applicationServer.network;

import applicationServerControlInterfaces.ApplicationServerControlInterface;
import gameInterfaces.clientGameInterfaces.ClientGameInterface;
import gameInterfaces.clientGameInterfaces.ClientLobbyInterface;
import dataClasses.Server;
import databaseAccountInterfaces.DatabaseAccountInterface;
import gameInterfaces.databaseGameInterfaces.DatabaseGameInterface;
import gameServerEntry.GameServerEntryInterface;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import static applicationServerControlInterfaces.ApplicationServerControlInterface.APPLICATION_CONTROL_NAME;

/**
 * Class to manage the network of application servers internally. All application servers know where all other
 * application servers are.
 */

public class ServerNetworkManager {

    // =================================================================================================================
    // logger ==========================================================================================================

    private static final Logger networkLogger = Logger.getLogger(ServerNetworkManager.class.getName());

    // =================================================================================================================
    // fields ==========================================================================================================

    private GameServerEntryInterface gameServerEntry;
    private List<Server> runningServers;
    private HashMap<Server, ApplicationServerControlInterface> runningServerControls;
    private DatabaseAccountInterface databaseAccountInterface;
    private DatabaseGameInterface databaseGameInterface;
    private ClientLobbyInterface lobbyInterface;
    private LinkedList<ClientGameInterface> localRunningGames;

    // =================================================================================================================
    // init ============================================================================================================

    public ServerNetworkManager(GameServerEntryInterface gameServerEntry) {
        this.gameServerEntry = gameServerEntry;
        this.runningServers = new LinkedList<>();
        this.runningServerControls = new HashMap<>();
        this.localRunningGames = new LinkedList<>();
    }

    // =================================================================================================================
    // getters 1 setters ===============================================================================================

    public GameServerEntryInterface getGameServerEntry() {
        return gameServerEntry;
    }

    public void setGameServerEntry(GameServerEntryInterface gameServerEntry) {
        this.gameServerEntry = gameServerEntry;
    }

    public List<Server> getRunningServers() {
        return runningServers;
    }

    public void setRunningServers(List<Server> runningServers) {
        this.runningServers = runningServers;
    }

    public HashMap<Server, ApplicationServerControlInterface> getRunningServerControls() {
        return runningServerControls;
    }

    public void setRunningServerControls(HashMap<Server, ApplicationServerControlInterface> runningServerControls) {
        this.runningServerControls = runningServerControls;
    }

    public DatabaseAccountInterface getDatabaseAccountInterface() {
        return databaseAccountInterface;
    }

    public void setDatabaseAccountInterface(DatabaseAccountInterface databaseAccountInterface) {
        this.databaseAccountInterface = databaseAccountInterface;
    }

    public DatabaseGameInterface getDatabaseGameInterface() {
        return databaseGameInterface;
    }

    public void setDatabaseGameInterface(DatabaseGameInterface databaseGameInterface) {
        this.databaseGameInterface = databaseGameInterface;
    }

    public ClientLobbyInterface getLobbyInterface() {
        return lobbyInterface;
    }

    public void setLobbyInterface(ClientLobbyInterface lobbyInterface) {
        this.lobbyInterface = lobbyInterface;
    }

    public LinkedList<ClientGameInterface> getLocalRunningGames() {
        return localRunningGames;
    }

    public void setLocalRunningGames(LinkedList<ClientGameInterface> localRunningGames) {
        this.localRunningGames = localRunningGames;
    }
    // =================================================================================================================
    // other methods ===================================================================================================

    /**
     * Method to add a new game to the local list of running games. Games are added as a {@link ClientGameInterface}
     * object.
     *
     * @param clientGameInterface   new game to add
     */
    public void addRunningGame(ClientGameInterface clientGameInterface) {
        this.localRunningGames.addLast(clientGameInterface);
    }

    /**
     * Method for server network manager to use it's list of {@link Server}s to establish a connection with each
     * running application server. If the application
     */
    public void connectAllRunningServers() {

        // temporary variables -----------------------------------------------------------------------------------------
        Registry registry;

        // attempt to connect to all running servers -------------------------------------------------------------------
        for (Server server: runningServers) {
            networkLogger.info(String.format("attempting connection to %s:%d", server.getHost(), server.getPort()));
            try {
                registry = LocateRegistry.getRegistry(server.getHost(), server.getPort());
                runningServerControls.put(
                        server,
                        (ApplicationServerControlInterface) registry.lookup(APPLICATION_CONTROL_NAME)
                );
            } catch (RemoteException e) {
                networkLogger.info(String.format(
                        "server at %s:%d might be down, removing from active servers...",
                        server.getHost(),
                        server.getPort()
                ));
                runningServers.remove(server);
                e.printStackTrace();
            } catch (NotBoundException e) {
                networkLogger.info(String.format(
                        "server at %s:%d might not be bound correctly, removing from active servers...",
                        server.getHost(),
                        server.getPort()
                ));
                runningServers.remove(server);
                e.printStackTrace();
            }
        }
    }
}
