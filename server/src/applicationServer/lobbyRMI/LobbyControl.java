package applicationServer.lobbyRMI;

import applicationServer.applicationRMI.GameControl;
import applicationServer.network.ServerNetworkManager;
import gameInterfaces.clientGameInterfaces.ClientGameInterface;
import gameInterfaces.clientGameInterfaces.ClientLobbyInterface;
import gameInterfaces.clientGameInterfaces.HostGameInterface;
import dataClasses.Server;
import gameInterfaces.databaseGameInterfaces.DatabaseGameInterface;
import exceptions.InvalidTokenException;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;
import java.util.logging.Logger;

import static applicationServer.ApplicationServer.applicationServerLogger;
import static gameInterfaces.clientGameInterfaces.ClientGameInterface.GAME_CONTROL_NAME;
import static gameInterfaces.clientGameInterfaces.ClientGameInterface.GAME_CONTROL_PORT_LOWER;
import static gameInterfaces.clientGameInterfaces.ClientGameInterface.GAME_CONTROL_PORT_UPPER;

public class LobbyControl extends UnicastRemoteObject implements ClientLobbyInterface{

    // =================================================================================================================
    // logger ==========================================================================================================
    private static final Logger lobbyLogger = Logger.getLogger(LobbyControl.class.getName());

    // =================================================================================================================
    // fields ==========================================================================================================
    private ServerNetworkManager applicationNetworkManager;
    private DatabaseGameInterface databaseGameInterface;
    private List<ClientGameInterface> runningGames;

    // =================================================================================================================
    // init ============================================================================================================
    public LobbyControl (
            ServerNetworkManager applicationNetworkManager,
            DatabaseGameInterface databaseGameInterface) throws RemoteException {
        init(applicationNetworkManager, databaseGameInterface);
    }

    private void init(
            ServerNetworkManager applicationServerList,
            DatabaseGameInterface databaseGameInterface){
        this.applicationNetworkManager = applicationServerList;
        this.databaseGameInterface = databaseGameInterface;
        this.runningGames = new LinkedList<>();

        new Timer().schedule(new updateGames(), 0, 10000);
    }

    // =================================================================================================================
    // interface overrides =============================================================================================

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUsername(String token) throws RemoteException, InvalidTokenException {
        return databaseGameInterface.getUsername(token);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ClientGameInterface> getRunningGames(String token) throws RemoteException, InvalidTokenException {
        if(databaseGameInterface.checkToken(token)) return runningGames;
        else return new LinkedList<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HostGameInterface createGame(String token, String name, int width, int height, int limit)
            throws RemoteException, InvalidTokenException {

        // only create game if user token checks out -------------------------------------------------------------------
        if(databaseGameInterface.checkToken(token)){
            applicationServerLogger.info(String.format(
                    "attempting to create game for user with token: %s...\n" +
                            "\tname: %s\n" +
                            "\tdimensions: %dx%d\n\t" +
                            "limit: %d",
                    token,
                    name,
                    width,
                    height,
                    limit
            ));

            // attempt to start a new game -----------------------------------------------------------------------------
            HostGameInterface hostGameInterface = null;
            for (int port = GAME_CONTROL_PORT_LOWER; port < GAME_CONTROL_PORT_UPPER; port++) {
                applicationServerLogger.info(String.format("attempting to start game on port %d...", port));
                try {
                    final Registry registry = LocateRegistry.createRegistry(port);
                    GameControl gameControl = new GameControl(
                            UUID.fromString(token), name, width, height, limit, databaseGameInterface
                    );
                    gameControl.setOnClose(() -> {
                        try {
                            runningGames.remove((ClientGameInterface) registry.lookup(GAME_CONTROL_NAME));
                            registry.unbind(GAME_CONTROL_NAME);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                            // probably no issues ----------------------------------------------------------------------
                        } catch (NotBoundException e) {
                            e.printStackTrace();
                            // also probably no issue ------------------------------------------------------------------
                        }
                    });
                    registry.rebind(GAME_CONTROL_NAME, gameControl);
                    hostGameInterface = (HostGameInterface) registry.lookup(GAME_CONTROL_NAME);
                    // add game to lobby's running games ---------------------------------------------------------------
                    applicationNetworkManager.addRunningGame((ClientGameInterface) hostGameInterface);
                    runningGames.add((ClientGameInterface) hostGameInterface);
                    applicationServerLogger.info(String.format("game started on port %d!", port));
                    break;
                } catch (RemoteException e) {
                    applicationServerLogger.info(String.format("port %d taken", port));
                } catch (NotBoundException e) {
                    applicationServerLogger.info("game might not be bound correctly");
                } catch (InvalidTokenException e) {
                    e.printStackTrace();
                }
            }
            return hostGameInterface;
        } else throw new InvalidTokenException();
    }

    // =================================================================================================================
    // lobby updater ===================================================================================================

    /**
     * {@link TimerTask} to refresh the lobby. This task will query all running application servers to get their lobby.
     */
    class updateGames extends TimerTask{

        @Override
        public void run() {
            List<ClientGameInterface> newInterfaces = new LinkedList<>();
            for(Server server: applicationNetworkManager.getRunningServerControls().keySet()){
                try {
                    newInterfaces.addAll(
                            applicationNetworkManager.getRunningServerControls().get(server).getRunningGames());
                } catch (RemoteException e) {
                    lobbyLogger.info(String.format(
                            "server on %s:%d might be down...\nremoving it now, %d server(s) remaining.",
                            server.getHost(),
                            server.getPort(),
                            applicationNetworkManager.getRunningServerControls().size()
                    ));
                    applicationNetworkManager.getRunningServerControls().remove(server);
                }
            }
            runningGames = newInterfaces;
        }
    }
}
