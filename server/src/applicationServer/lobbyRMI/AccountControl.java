package applicationServer.lobbyRMI;

import applicationServer.network.ServerNetworkManager;
import clientAccountInterfaces.ClientAccountInterface;
import gameInterfaces.clientGameInterfaces.ClientLobbyInterface;
import exceptions.*;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.logging.Logger;

public class AccountControl extends UnicastRemoteObject implements ClientAccountInterface {

    // =================================================================================================================
    // logger ==========================================================================================================
    private static final Logger accountLogger = Logger.getLogger(AccountControl.class.getName());

    // =================================================================================================================
    // local variables =================================================================================================
    private ServerNetworkManager serverNetworkManager;

    // =================================================================================================================
    // init ============================================================================================================
    public AccountControl(ServerNetworkManager serverNetworkManager) throws RemoteException {
        this.serverNetworkManager = serverNetworkManager;
    }

    // =================================================================================================================
    // interface implementations =======================================================================================
    /**
     * {@inheritDoc}
     */
    @Override
    public String login(String username, String password) throws RemoteException, AccountNotFoundException, WrongPasswordException {
        accountLogger.info("received login request from: " + username);
        serverNetworkManager.getDatabaseAccountInterface().checkLogin(username, password);
        return serverNetworkManager.getDatabaseAccountInterface().getToken(username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean checkLogin(String token) throws RemoteException, InvalidTokenException, AccountNotFoundException {
        accountLogger.info(String.format("checking login with token %s", token));
        return serverNetworkManager.getDatabaseAccountInterface().checkLogin(token);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean register(String username, String password, String email) throws RemoteException, UsernameAlreadyExistsException, EmailAddressAlreadyInUseException {
        accountLogger.info("received register request from: " + username);
        try {
            return serverNetworkManager.getDatabaseAccountInterface().createAccount(username, password, email);
        } catch (RemoteException e){
            accountLogger.severe("could not create user.\n");
            e.printStackTrace();
        } catch (UsernameAlreadyExistsException e) {
            accountLogger.severe("could not create user.\n");
            e.printStackTrace();
            throw new UsernameAlreadyExistsException(username);
        } catch (EmailAddressAlreadyInUseException e) {
            accountLogger.severe("could not create user.\n");
            e.printStackTrace();
            throw new EmailAddressAlreadyInUseException(email);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean removeAccount(String token) throws RemoteException {
        accountLogger.info("received account removal request from: " + token);
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void logout(String token) throws RemoteException{
        try {
            serverNetworkManager.getDatabaseAccountInterface().remove(token);
        } catch (RemoteException e) {
            accountLogger.info("problem at database...\n");
            e.printStackTrace();
        } catch (AccountNotFoundException e) {
            accountLogger.info("account not found, so no token to delete anyway!\n");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ClientLobbyInterface getLobby(String token) throws RemoteException, InvalidTokenException {
        return serverNetworkManager.getLobbyInterface();
    }
}
