package databaseEntry;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface DatabaseEntryInterface extends Remote {

    // =================================================================================================================
    // static constants ================================================================================================

    String DATABASE_ENTRY_HOST = "localhost";
    int DATABASE_ENTRY_PORT = 12100;
    String DATABASE_ENTRY_NAME = "database_entry";

    // =================================================================================================================
    // methods =========================================================================================================

    /**
     * Method for database server to call after startup to signal the broker it is live. The broker will add it to
     * it's list of active database servers
     *
     * @param hostname  database host name
     * @param port      port the database server is running on
     * @throws RemoteException  default rmi exception
     */
    void enter(String hostname, int port) throws RemoteException;
}
