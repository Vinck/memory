package dataClasses;

import java.io.Serializable;

public class Server implements Serializable {

    // =================================================================================================================
    // variable ========================================================================================================
    private String host;
    private int port;

    // =================================================================================================================
    // init ============================================================================================================

    public Server(String host, int port) {
        this.host = host;
        this.port = port;
    }

    // =================================================================================================================
    // getters qnd setters =============================================================================================

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
