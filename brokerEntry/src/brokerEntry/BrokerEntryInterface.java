package brokerEntry;

import clientAccountInterfaces.ClientAccountInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface BrokerEntryInterface extends Remote{

    // =================================================================================================================
    // static values ===================================================================================================
    String BROKER_HOST = "localhost";
    int BROKER_PORT = 12000;
    String BROKER_NAME = "broker";

    // =================================================================================================================
    // methods =========================================================================================================
    /**
     * Method to handle the request of a client to enter the system. Dispatcher will assign client to a certain
     * application server for login/registering and to enter games afterwards.
     *
     * @return  account handler on assigned application server
     * @throws RemoteException  default rmi exception
     */
    ClientAccountInterface enter() throws RemoteException;
}
