package gameServerEntry;

import dataClasses.Server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface GameServerEntryInterface extends Remote {

    // =================================================================================================================
    // static values ===================================================================================================
    String GAME_ENTRY_HOST = "localhost";
    int GAME_ENTRY_PORT = 12200;
    String GAME_ENTRY_NAME = "game_entry";

    // =================================================================================================================
    // methods =========================================================================================================

    /**
     * Method for new game server to call to signal it is up and running. Broker will register it as an available
     * server and will give it a list of all running application servers.
     *
     * @param hostname  the host name of the newly started server
     * @param port      the port the new server was started on
     *
     * @return          the data needed to find all application servers
     * @throws RemoteException  default rmi exception
     */
    List<Server> enterApplicationServerControl(String hostname, int port) throws RemoteException;

    /**
     * Method for new game server to call to signal it is up and running. Broker will assign a database server to it
     * and register it in it's class to manage the available servers for clients.
     *
     * @param hostname  the host name of the newly started server
     * @param port      the port the new server was started on
     *
     * @return          the data needed to find a database server
     * @throws RemoteException  default rmi exception
     */
    Server enterAccountControl(String hostname, int port) throws RemoteException;

    /**
     * Method for game server to call in order to get a new database server in case it detects it's connected server
     * is down. The server manager will update it's list to remove the downed server and try restarting one.
     *
     * @param hostname  the host name of the down database server
     * @param port      the port the down database server was connected on
     *
     * @return          the data needed to find the new database server
     * @throws RemoteException  default rmi exception
     */
    Server requestNewDatabase(String hostname, int port) throws RemoteException;
}
