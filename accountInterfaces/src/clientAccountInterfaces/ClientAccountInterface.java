package clientAccountInterfaces;

import gameInterfaces.clientGameInterfaces.ClientLobbyInterface;
import exceptions.*;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ClientAccountInterface extends Remote{

    // =================================================================================================================
    // static values ===================================================================================================
    String ACCOUNTS_HOST = "localhost";
    int ACCOUNTS_PORT_LOWER = 11000;
    int ACCOUNTS_PORT_UPPER = 11499;
    String ACCOUNTS_NAME = "accounts";

    // =================================================================================================================
    // methods =========================================================================================================

    /**
     * Method to handle loin request from user. Application server will have to forward request to database server's
     * {@link databaseAccountInterfaces.DatabaseAccountInterface#checkLogin(String, String)} method and will return
     * the token assigned to the user, if the user's credentials check out.
     *
     * @param username  username of the user trying to log in
     * @param password  user's password
     *
     * @return          assigned token, when credentials are correct
     * @throws RemoteException          default rmi exception
     * @throws AccountNotFoundException exception thrown when database server can't find the specified account
     * @throws WrongPasswordException   exception thrown when the database detects that the given password was incorrect
     */
    String login(String username, String password) throws RemoteException, AccountNotFoundException, WrongPasswordException;

    /**
     * Method to check if a user's token is valid, this method is used to grant the user access to the lobby. The client
     * will first have to acquire a token from the {@link #login(String, String)} method. This method will forward the
     * request to the {@link databaseAccountInterfaces.DatabaseAccountInterface#checkLogin(String)} method.
     *
     * @param token user's unique token
     *
     * @return      true if token check's out, false otherwise
     * @throws RemoteException          default rmi exception
     * @throws InvalidTokenException    thrown when the database detects an invalid token
     * @throws AccountNotFoundException thrown when the database cannot find the given token
     */
    boolean checkLogin(String token) throws RemoteException, InvalidTokenException, AccountNotFoundException;

    /**
     * Method to user for client applications in order to register for an account. This call will be forwarded to the
     * database server's
     * {@link databaseAccountInterfaces.DatabaseAccountInterface#createAccount(String, String, String)}
     * method. A username, password and email are required. The password is provided in plain text and will be hashed
     * at the database side.
     *
     * @param username  the user's chosen username
     * @param password  the user's specified password
     * @param email     the user's specified email
     *
     * @return          true if the registration is successful
     * @throws RemoteException                      default rmi exception
     * @throws UsernameAlreadyExistsException       exception thrown when the database detects a duplicate username
     * @throws EmailAddressAlreadyInUseException    exception thrown when the database detects a duplicate email
     */
    boolean register(
            String username,
            String password,
            String email
    ) throws RemoteException, UsernameAlreadyExistsException, EmailAddressAlreadyInUseException;

    /**
     * Method to be called when a user wants to delete his account. request will be forwarded to
     * {@link databaseAccountInterfaces.DatabaseAccountInterface#remove(String)} at the database server. This method
     * requires the user to be logged in and in possession  of his unique token.
     *
     * @param token the user's unique token
     *
     * @return      true on success, false otherwise
     * @throws RemoteException  default rmi exception
     */
    boolean removeAccount(String token) throws RemoteException;

    /**
     * Method to be called when a user wants to log out. This request will be forwarded to the database server's
     * {@link databaseAccountInterfaces.DatabaseAccountInterface#remove(String)} method to remove the token from
     * the database.
     *
     * @param token the user's unique identification token
     *
     * @throws RemoteException  default rmi exception
     */
    void logout(String token) throws RemoteException;

    /**
     * Method for client to call to connect to the lobby. The separation of the interfaces makes sure the lobby can
     * only be accessed by an authorized user.
     *
     * @param token the user's unique token
     * @return      a connection to the lobby
     * @throws RemoteException          default rmi exceptions
     * @throws InvalidTokenException    thrown when the database detects an invalid token
     */
    ClientLobbyInterface getLobby(String token) throws RemoteException, InvalidTokenException;
}
