package databaseAccountInterfaces;

import gameInterfaces.databaseGameInterfaces.DatabaseGameInterface;
import exceptions.*;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface DatabaseAccountInterface extends Remote{

    String DATABASE_ACCOUNTS_HOST = "localhost";
    int DATABASE_ACCOUNTS_PORT_LOWER = 10500;
    int DATABASE_ACCOUNTS_PORT_UPPER = 10999;
    String DATABASE_ACCOUNTS_NAME = "accounts";

    /**
     * Method to handle login request that was forwarded from the application server's
     * {@link clientAccountInterfaces.ClientAccountInterface#checkLogin(String)} method to the database server. The
     * method will check a specified token against it's database to verify it's validity and will return true if the
     * token is valid.
     *
     * @param token user's unique token
     * @return      true if token is valid
     * @throws RemoteException          default rmi exception
     * @throws AccountNotFoundException thrown when no account is found to be coupled to the token
     * @throws InvalidTokenException    thrown when the token is no longer valid
     */
    boolean checkLogin(String token) throws RemoteException, AccountNotFoundException, InvalidTokenException;

    /**
     * Method to check a user's credentials. This method will be called to forward a login request sent to the
     * application servers's {@link clientAccountInterfaces.ClientAccountInterface#login(String, String)} method.
     * This method will check the user's credentials against it's database and return true if the credentials check
     * out.
     *
     * @param username  the user's unique username
     * @param password  the user's secret password
     * @throws RemoteException          default rmi exception
     * @throws AccountNotFoundException thrown when no account can be found matching the username
     * @throws WrongPasswordException   thrown when the specified password does not match the database
     */
    void checkLogin(String username, String password)
            throws RemoteException, AccountNotFoundException, WrongPasswordException;

    /**
     * Method to acquire a token for a specific user. This method should only be used on the application server to
     * acquire a token after the user's credentials were checked.
     *
     * @param username  user's unique username
     * @return          a unique token to identify the user
     * @throws RemoteException          default rmi exception
     * @throws AccountNotFoundException thrown when the database cannot find the given account
     */
    String getToken(String username) throws RemoteException, AccountNotFoundException;

    /**
     * Method for server to call to forward a request for
     * {@link clientAccountInterfaces.ClientAccountInterface#register(String, String, String)} to. This method will
     * check the parameters for the account and persist a new user to the database if the parameters can be user.
     *
     * @param username  a unique user name
     * @param password  a password
     * @param email     an email address
     * @return          true if successful
     * @throws RemoteException                      default rmi exception
     * @throws UsernameAlreadyExistsException       thrown when the given username is already in use
     * @throws EmailAddressAlreadyInUseException    thrown when the given email address is already in use
     */
    boolean createAccount(
            String username,
            String password,
            String email
    ) throws RemoteException, UsernameAlreadyExistsException, EmailAddressAlreadyInUseException;

    /**
     * Method to be able to remove a given account given a specific unique token. This method is technically a
     * serious security flaw and is not implemented at the point in time.
     *
     * @param token user's unique token
     * @return      true if the token has been removed
     * @throws RemoteException          default rmi exception
     * @throws AccountNotFoundException thrown when there's no account found coupled to token
     */
    boolean remove(String token) throws RemoteException, AccountNotFoundException;

    /**
     * Method to get a running game interface on the server. Every running database server has both a
     * {@link DatabaseAccountInterface} implementation and a {@link DatabaseGameInterface}, the account interface is
     * the entry point into the database server for the application server, so this interface implementation will be
     * able to supply the application server with an entry into the game management at the database.
     *
     * @return                  connection to the database's game interface
     * @throws RemoteException  default rmi exception
     */
    DatabaseGameInterface getDatabaseGameInterface() throws RemoteException;
}
