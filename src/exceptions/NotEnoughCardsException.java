package exceptions;

public class NotEnoughCardsException extends Exception{
    public NotEnoughCardsException() {
        super("not enough cards added to game;");
    }

    public NotEnoughCardsException(String message) {
        super(message);
    }
}
