package exceptions;

public class FullGameException extends Exception{
    public FullGameException() {
        super("game already full.");
    }

    public FullGameException(String message) {
        super(message);
    }
}
