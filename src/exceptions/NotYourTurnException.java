package exceptions;

public class NotYourTurnException extends Exception{
    public NotYourTurnException() {
        super("it doesn't appear to be your turn.");
    }

    public NotYourTurnException(String message) {
        super(message);
    }
}
