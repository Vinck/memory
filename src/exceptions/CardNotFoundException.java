package exceptions;

public class CardNotFoundException extends Exception{
    public CardNotFoundException() {
        super("card could not be found");
    }

    public CardNotFoundException(String message) {
        super(message);
    }
}
