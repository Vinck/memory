package exceptions;

public class BrokerConnectionException extends Exception{

    public BrokerConnectionException() {
        super("unable to connect to broker.");
    }

    public BrokerConnectionException(String message) {
        super(message);
    }
}
