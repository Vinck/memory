package exceptions;

public class GameNotOpenException extends Exception{
    public GameNotOpenException() {
    }

    public GameNotOpenException(String message) {
        super(message);
    }
}
