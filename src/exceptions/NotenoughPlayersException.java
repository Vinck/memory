package exceptions;

public class NotenoughPlayersException extends Exception{
    public NotenoughPlayersException() {
    }

    public NotenoughPlayersException(String message) {
        super(message);
    }
}
