package exceptions;

public class InvalidTokenException extends Exception{
    public InvalidTokenException() {
        super("token no longer valid");
    }

    public InvalidTokenException(String message) {
        super(message);
    }
}
