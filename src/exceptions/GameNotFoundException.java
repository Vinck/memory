package exceptions;

public class GameNotFoundException extends Exception{
    public GameNotFoundException() {
        super("game not found");
    }

    public GameNotFoundException(String message) {
        super(message);
    }
}
