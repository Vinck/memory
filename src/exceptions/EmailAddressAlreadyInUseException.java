package exceptions;

public class EmailAddressAlreadyInUseException extends Exception{
    public EmailAddressAlreadyInUseException() {
        super("email address registered");
    }

    public EmailAddressAlreadyInUseException(String message) {
        super(message);
    }
}
