package exceptions;

public class UsernameAlreadyExistsException extends Exception{
    public UsernameAlreadyExistsException() {
        super("This username is already in use");
    }

    public UsernameAlreadyExistsException(String message) {
        super(message);
    }
}
