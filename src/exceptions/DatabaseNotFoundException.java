package exceptions;

public class DatabaseNotFoundException extends Exception {
    public DatabaseNotFoundException() {
        super("database not found.");
    }

    public DatabaseNotFoundException(String message) {
        super(message);
    }
}
