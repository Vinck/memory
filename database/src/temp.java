import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;

//xxxxxxxx-xxxx-Mxxx-Nxxx-xxxxxxxxxxxx
//00000001-0000-0000-0000-000000000000

public class temp {
    public static void main(String[] args){
        System.out.println("Insert Image Example!");
        String driverName = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "memory_db";
        String userName = "memory_db_connection";
        String password = "MichielIsCool";
        Connection con = null;
        try{
            Class.forName(driverName);
            con = DriverManager.getConnection(url+dbName,userName,password);
            Statement st = con.createStatement();
            File imgfile = new File("C:\\dataFolder\\programmeren\\java\\Memory\\client\\src\\resources\\data\\StandardFace7.png");

            FileInputStream fin = new FileInputStream(imgfile);

            PreparedStatement pre =
                    con.prepareStatement("insert into cards values(?,?)");

            pre.setString(1,"00000007-0000-0000-0000-000000000000");
            pre.setBinaryStream(2,(InputStream)fin,(int)imgfile.length());
            pre.executeUpdate();
            System.out.println("Successfully inserted the file into the database!");

            pre.close();
            con.close();
        }catch (Exception e1){
            System.out.println("error");
            e1.printStackTrace();
            System.out.println(e1.getMessage());
        }
    }
}
