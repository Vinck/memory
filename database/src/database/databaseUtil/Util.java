package database.databaseUtil;

import gameInterfaces.dataClasses.Card;
import database.persistence.Game;
import database.persistence.Token;
import org.hibernate.Session;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.persistence.Query;
import java.util.*;

import static database.Database.databaseLogger;
import static database.persistence.Token.FIND_TOKEN_QUERY;

/**
 * Method used by the database to perform a variety of tasks. The static methods in the class server to prevent
 * unnecessary code duplication.
 */

public class Util {

    /**
     * Method used to generate new random token using {@link UUID#randomUUID()}. Tokens are always stored as String
     * objects
     *
     * @return  String representation of {@link UUID} token
     */
    public static String generateToken(){
        return UUID.randomUUID().toString();
    }

    /**
     * Method to check the validity of a certain user's unique token.
     *
     * @param token     user's token
     * @param session   session to generate query from
     *
     * @return          true if the token is still valid
     */
    public static boolean checkToken(String token, Session session){
        boolean valid = false;
        Query query = session.createQuery(String.format(FIND_TOKEN_QUERY, token));
        List tokens = query.getResultList();
        if(tokens.size() > 1){
            // shouldn't happen.
            databaseLogger.severe("duplicate tokens");
            return false;
        } else if (tokens.size() != 0) {
            valid = ((Token) tokens.get(0)).isValid(new Date());
        }

        session.close();
        databaseLogger.info(String.format(valid ?
                "token %s is valid" : "token %s is no longer valid", token));
        return valid;
    }

    /**
     * Method used to generate a new card id. Card ids are represented by {@link UUID}s as a String. The database will
     * be queried in order to make sure tokens aren't duplicated, even though this should only very rarely happen.
     *
     * @param session   {@link Session} object to be able to perform actions on the database
     *
     * @return          new {@link UUID} for card id
     */
    public static UUID generateCardId(Session session){
        UUID uuid;
        do {
            uuid = UUID.randomUUID();
        } while (session.get(Card.class, uuid.toString()) != null);
        return uuid;
    }

    /**
     * Method used to generate a new unique game id. Game ids are represented by {@link UUID}s as a String. The
     * database will be queried in order to make sure the game id is not duplicated, this should only very rarely
     * happen.
     *
     * @param session   {@link Session} object to be able to perform actions on the database
     *
     * @return          new {@link UUID} for game id
     */
    public static UUID generateGameId(Session session){
        UUID uuid;
        do {
            uuid = UUID.randomUUID();
        } while (session.find(Game.class, uuid.toString())!= null);
        return uuid;
    }

    /**
     * Method used to store all information about the games scoreboard in a JSON representation. This can be used to
     * reconstruct the scoreboard and find out which players where active in the game, as the unique username is stored.
     * The game's scores are passed in the form of a {@link HashMap} with the player's username as the key and the
     * player's score as the value. The JSON representation is done with a {@link JSONArray} object and follows the
     * structure:
     * <pre>
     *     [
     *          {username:"username", score:"score"},
     *          ...
     *     ]
     * </pre>
     *
     * @param scores    the game's scores
     *
     * @return          the JSON representation
     */
    public static JSONArray generateScoresArray(HashMap<String, Integer> scores) {
        JSONArray scoresArray = new JSONArray();
        JSONObject jsonObject;
        for(String username: scores.keySet()){
            jsonObject = new JSONObject();
            jsonObject.put("username", username);
            jsonObject.put("score", scores.get(username));
            scoresArray.put(jsonObject);
        }
        return scoresArray;
    }

    /**
     * Method to store all of a game's information concerning the field in a JSON representation. This can be used to
     * reconstruct the game field, including which cards have been flipped and even the solution. The faces are stored
     * as their id so they can be retrieved from the database. the JSON representation is done with a {@link JSONArray}
     * object, following the structure:
     * <pre>
     *     [
     *          {face:"card id", flipped:"true or false"},
     *          ...
     *     ]
     * </pre>
     *
     * @param cards     list of card faces
     * @param faces     list of faces ids
     * @param solution  the game's solution
     *
     * @return          the generated JSONArray representation
     */
    public static JSONArray generateFacesArray(ArrayList<Card> cards, ArrayList<UUID> faces, int[] solution){
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject;
        for (Card card: cards){
            jsonObject = new JSONObject();
            jsonObject.put("face", faces.get(solution[cards.indexOf(card)]));
            jsonObject.put("flipped", card.isFlipped());
            jsonArray.put(jsonObject);
        }
        return jsonArray;
    }
}
