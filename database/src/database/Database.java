package database;

import database.databaseRMI.DatabaseConnection;
import database.databaseRMI.DatabaseGameControl;
import database.persistence.Card;
import databaseEntry.DatabaseEntryInterface;
import exceptions.DatabaseNotFoundException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import java.util.logging.Logger;

import static databaseAccountInterfaces.DatabaseAccountInterface.*;
import static databaseEntry.DatabaseEntryInterface.DATABASE_ENTRY_HOST;
import static databaseEntry.DatabaseEntryInterface.DATABASE_ENTRY_NAME;
import static databaseEntry.DatabaseEntryInterface.DATABASE_ENTRY_PORT;
import static gameInterfaces.databaseGameInterfaces.DatabaseGameInterface.*;
import static gameInterfaces.util.Util.rescaleGetBytes;

public class Database {

    // =================================================================================================================
    // static values ===================================================================================================

    public static final String STANDARD_BACK = "00000000-0000-0000-0000-000000000000";
    public static final String STANDARD_FACE_1 = "00000001-0000-0000-0000-000000000000";
    public static final String STANDARD_FACE_2 = "00000002-0000-0000-0000-000000000000";
    public static final String STANDARD_FACE_3 = "00000003-0000-0000-0000-000000000000";
    public static final String STANDARD_FACE_4 = "00000004-0000-0000-0000-000000000000";
    public static final String STANDARD_FACE_5 = "00000005-0000-0000-0000-000000000000";
    public static final String STANDARD_FACE_6 = "00000006-0000-0000-0000-000000000000";
    public static final String STANDARD_FACE_7 = "00000007-0000-0000-0000-000000000000";

    // =================================================================================================================
    // logger ==========================================================================================================

    public static final Logger databaseLogger = Logger.getLogger("database");

    // =================================================================================================================
    // run =============================================================================================================

    public static void main(String[] args){

        databaseLogger.info("starting database...");

        // making sure default cards are present -----------------------------------------------------------------------
        loadDefaultCards();

        // temporary variables -----------------------------------------------------------------------------------------
        int gamesPort = -1;
        int accountPort = -1;
        DatabaseConnection databaseConnection = null;

        // start account manager ---------------------------------------------------------------------------------------
        Registry databaseAccountRegistry;
        for (int port = DATABASE_ACCOUNTS_PORT_LOWER; port < DATABASE_ACCOUNTS_PORT_UPPER; port++) {
            databaseLogger.info(String.format(
                    "attempting to start account control on port %d",
                    port
            ));
            try {
                databaseAccountRegistry = LocateRegistry.createRegistry(port);
                databaseConnection = new DatabaseConnection();
                databaseAccountRegistry.rebind(DATABASE_ACCOUNTS_NAME, databaseConnection);
                accountPort = port;
                databaseLogger.info(String.format(
                        "database interface for accounts started! on port %d",
                        port
                ));
                break;
            } catch (RemoteException e) {
                databaseLogger.severe("unable to start database accounts interface\n" + e.getMessage());
            } catch (DatabaseNotFoundException e) {
                databaseLogger.severe("unable to connect to internal database server\n" + e.getMessage());
            }
        }

        // break of database not started -------------------------------------------------------------------------------
        if (databaseConnection == null) {
            databaseLogger.severe("could not start database...");
            return;
        }

        // start game manager ------------------------------------------------------------------------------------------
        Registry databaseGameRegistry;
        for (int port = DATABASE_GAME_PORT_LOWER; port < DATABASE_GAME_PORT_UPPER; port++) {
            databaseLogger.info(String.format(
                    "attempting to start game control on port %d",
                    port
            ));
            try {
                databaseGameRegistry = LocateRegistry.createRegistry(port);
                databaseConnection.setDatabaseGameInterface(new DatabaseGameControl());
                databaseGameRegistry.rebind(DATABASE_GAME_NAME, databaseConnection.getDatabaseGameInterface());
                gamesPort = port;
                databaseLogger.info(String.format(
                        "database interface for games started started! on port %d",
                        port
                ));
                break;
            } catch (RemoteException e) {
                databaseLogger.severe("unable to start database game interface\n" + e.getMessage());
            } catch (DatabaseNotFoundException e) {
                databaseLogger.severe("unable to connect to internal database server\n" + e.getMessage());
            }
        }

        if (gamesPort != -1 && accountPort != -1) {
            // register with broker ------------------------------------------------------------------------------------
            try {
                Registry registry = LocateRegistry.getRegistry(DATABASE_ENTRY_HOST, DATABASE_ENTRY_PORT);
                DatabaseEntryInterface databaseEntry = (DatabaseEntryInterface) registry.lookup(DATABASE_ENTRY_NAME);
                databaseEntry.enter(DATABASE_ACCOUNTS_HOST, accountPort);
            } catch (RemoteException e) {
                databaseLogger.severe("problem signalling broker...");
            } catch (NotBoundException e) {
                databaseLogger.severe("broker not found...");
            }
        }
    }

    /**
     * Method to insert default card faces and default card back into database. If a card is found with a default id
     * it will be overwritten, this makes sure the right default is in the database in case the default changes at a
     * later time.
     */
    private static void loadDefaultCards() {

        databaseLogger.info("inserting defaults that aren't in the database...");

        Session session = null;
        Transaction transaction = null;
        try {
            // prepare for database access -----------------------------------------------------------------------------
            SessionFactory factory = new Configuration().configure().buildSessionFactory();
            session = factory.openSession();
            transaction = session.beginTransaction();

            // default back --------------------------------------------------------------------------------------------
            Card back = session.find(Card.class, STANDARD_BACK);
            if (back == null) back = new Card(STANDARD_BACK, null);
            back.setImage(rescaleGetBytes(
                    "C:\\dataFolder\\programmeren\\java\\Memory\\database\\src\\database\\defaultFaces\\" +
                            "MemoryStandardCardBack.png"
            ));
            session.save(back);
            databaseLogger.info("default back updated");

            // default face 1 ------------------------------------------------------------------------------------------
            Card face1 = session.find(Card.class, STANDARD_FACE_1);
            if (face1 == null) face1 = new Card(STANDARD_FACE_1, null);
            face1.setImage(rescaleGetBytes(
                    "C:\\dataFolder\\programmeren\\java\\Memory\\database\\src\\database\\defaultFaces\\" +
                            "StandardFace1.png"
            ));
            session.save(face1);
            databaseLogger.info("default face 1 updated");

            // default face 2 ------------------------------------------------------------------------------------------
            Card face2 = session.find(Card.class, STANDARD_FACE_2);
            if (face2 == null) face2 = new Card(STANDARD_FACE_2, null);
            face2.setImage(rescaleGetBytes(
                    "C:\\dataFolder\\programmeren\\java\\Memory\\database\\src\\database\\defaultFaces\\" +
                            "StandardFace2.png"
            ));
            session.save(face2);
            databaseLogger.info("default face 2 updated");

            // default face 3 ------------------------------------------------------------------------------------------
            Card face3 = session.find(Card.class, STANDARD_FACE_3);
            if (face3 == null) face3 = new Card(STANDARD_FACE_3, null);
            face3.setImage(rescaleGetBytes(
                    "C:\\dataFolder\\programmeren\\java\\Memory\\database\\src\\database\\defaultFaces\\" +
                            "StandardFace3.png"
            ));
            session.save(face3);
            databaseLogger.info("default face 3 updated");

            // default face 4 ------------------------------------------------------------------------------------------
            Card face4 = session.find(Card.class, STANDARD_FACE_4);
            if (face4 == null) face4 = new Card(STANDARD_FACE_4, null);
            face4.setImage(rescaleGetBytes(
                    "C:\\dataFolder\\programmeren\\java\\Memory\\database\\src\\database\\defaultFaces\\" +
                            "StandardFace4.png"
            ));
            session.save(face4);
            databaseLogger.info("default face 4 updated");

            // default face 5 ------------------------------------------------------------------------------------------
            Card face5 = session.find(Card.class, STANDARD_FACE_5);
            if (face5 == null) face5 = new Card(STANDARD_FACE_5, null);
            face5.setImage(rescaleGetBytes(
                    "C:\\dataFolder\\programmeren\\java\\Memory\\database\\src\\database\\defaultFaces\\" +
                            "StandardFace5.png"
            ));
            session.save(face5);
            databaseLogger.info("default face 5 updated");

            // default face 6 ------------------------------------------------------------------------------------------
            Card face6 = session.find(Card.class, STANDARD_FACE_6);
            if (face6 == null) face6 = new Card(STANDARD_FACE_6, null);
            face6.setImage(rescaleGetBytes(
                    "C:\\dataFolder\\programmeren\\java\\Memory\\database\\src\\database\\defaultFaces\\" +
                            "StandardFace6.png"
            ));
            session.save(face6);
            databaseLogger.info("default face 6 updated");

            // default face 7 ------------------------------------------------------------------------------------------
            Card face7 = session.find(Card.class, STANDARD_FACE_7);
            if (face7 == null) face7 = new Card(STANDARD_FACE_7, null);
            face7.setImage(rescaleGetBytes(
                    "C:\\dataFolder\\programmeren\\java\\Memory\\database\\src\\database\\defaultFaces\\" +
                            "StandardFace7.png"
            ));
            session.save(face7);
            databaseLogger.info("default face 7 updated");

            databaseLogger.info("all card defaults updated!");
        } catch (Exception e){
            if (transaction != null) {
                transaction.rollback();
                transaction = null;
            }
            databaseLogger.severe("problem inserting defaults into database...");
            databaseLogger.severe(e.getMessage());
        } finally {
            if (transaction != null) transaction.commit();
            if (session != null) session.close();
        }
    }
}
