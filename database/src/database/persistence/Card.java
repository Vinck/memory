package database.persistence;

import javax.persistence.*;

@Entity
@Table(name = "cards")
public class Card {

    @Id
    @Column(name = "card_id", nullable = false)
    private String imageId;
    @Column(name = "image", nullable = false)
    private byte[] image;

    public Card(String imageId, byte[] image) {
        this.imageId = imageId;
        this.image = image;
    }

    public Card() {
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
