package database.persistence;

import javax.persistence.*;

@Entity
@Table(name = "games")
public class Game {

    @Id
    @Column(name = "game_id", nullable = false)
    private String id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "host", nullable = false)
    private String host;
    @Column(name = "width", nullable = false)
    private int width;
    @Column(name = "height", nullable = false)
    private int height;
    @Column(name = "player_limit", nullable = false)
    private int limit;
    @Column(name = "card_back", nullable = false)
    private String back_id;
    @Column(name = "card_faces", nullable = false)
    private String card_faces;
    @Column(name = "scores", nullable = false)
    private String scores;

    public Game(String id, String name, String host, int width, int height, int limit, String back_id, String card_faces, String scores) {
        this.id = id;
        this.name = name;
        this.host = host;
        this.width = width;
        this.height = height;
        this.limit = limit;
        this.back_id = back_id;
        this.card_faces = card_faces;
        this.scores = scores;
    }

    public Game() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public String getBack_id() {
        return back_id;
    }

    public void setBack_id(String back_id) {
        this.back_id = back_id;
    }

    public String getCard_faces() {
        return card_faces;
    }

    public void setCard_faces(String card_faces) {
        this.card_faces = card_faces;
    }

    public String getScores() {
        return scores;
    }

    public void setScores(String scores) {
        this.scores = scores;
    }
}
