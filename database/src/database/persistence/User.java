package database.persistence;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class User {
    public static final String FIND_USER_EMAIL_QUERY = "select u from User u where u.email = \'%s\'";

    @Id
    @Column(name = "username",nullable = false)
    private String username;
    @Column(name = "password", nullable = false)
    private String password;
    @Column(name = "salt", nullable = false)
    private String salt;
    @Column(name = "email",nullable = false)
    private String email;

    public User() {
    }

    public User(String username, String password, String salt, String email) {
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
}