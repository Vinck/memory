package database.persistence;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tokens")
public class Token {

    public static final String FIND_TOKEN_QUERY ="select token from Token token where token.token = \'%s\'";

    public static long TOKEN_VALID = 3600000;

    @Id
    @Column(name = "username", nullable = false)
    private String username;
    @Column(name = "token", nullable = false, unique = true)
    private String token;
    @Column(name = "token_valid", nullable = false)
    private Date tokenValid;

    public Token(String username, String token, Date tokenValid) {
        this.username = username;
        this.token = token;
        this.tokenValid = tokenValid;
    }

    public Token() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getTokenValid() {
        return tokenValid;
    }

    public void setTokenValid(Date tokenvalid) {
        this.tokenValid = tokenvalid;
    }

    public boolean isValid(Date date) {
        return date.getTime() < tokenValid.getTime();
    }
}
