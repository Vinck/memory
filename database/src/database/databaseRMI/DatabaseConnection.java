package database.databaseRMI;

import databaseAccountInterfaces.DatabaseAccountInterface;
import database.persistence.Token;
import database.persistence.User;
import gameInterfaces.databaseGameInterfaces.DatabaseGameInterface;
import exceptions.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.TransactionException;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;
import java.util.Date;

import static database.Database.databaseLogger;
import static database.databaseUtil.Util.*;
import static database.persistence.Token.FIND_TOKEN_QUERY;
import static database.persistence.User.FIND_USER_EMAIL_QUERY;
import static util.AccountUtil.getSalt;
import static util.AccountUtil.hashPassword;

public class DatabaseConnection extends UnicastRemoteObject implements DatabaseAccountInterface {

    // =================================================================================================================
    // fields ==========================================================================================================

    private SessionFactory factory;
    private DatabaseGameInterface databaseGameInterface;

    // =================================================================================================================
    // init ============================================================================================================

    public DatabaseConnection() throws RemoteException, DatabaseNotFoundException {
        super();
        init();
    }

    /**
     * sets the {@link DatabaseConnection#factory}.
     *
     * @throws DatabaseNotFoundException    thrown when the local database cannot be found
     */
    private void init() throws DatabaseNotFoundException {
        try {
            Configuration configuration = new Configuration();
            configuration.configure();
            factory = configuration.buildSessionFactory();
            Session lookup = factory.openSession();
            for (Object item: lookup.createQuery("select token FROM Token token").getResultList()) {
                try {
                    databaseLogger.info(String.format(
                            "\nfound token %s for user %s\n",
                            ((Token) item).getToken(),
                            ((Token) item).getUsername()
                    ));
                } catch (ClassCastException e) {

                }
            }
            lookup.close();
        } catch (Exception e){
            throw new DatabaseNotFoundException(e.getMessage());
        }
    }

    // =================================================================================================================
    // setter for game interface =======================================================================================

    public void setDatabaseGameInterface(DatabaseGameInterface databaseGameInterface) {
        this.databaseGameInterface = databaseGameInterface;
    }

    // =================================================================================================================
    // interface overrides =============================================================================================

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean checkLogin(String token) throws RemoteException, AccountNotFoundException, InvalidTokenException {
        databaseLogger.info("checking login for token: " + token);
        Session session = factory.openSession();
        return checkToken(token, session);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkLogin(String username, String password) throws RemoteException, AccountNotFoundException, WrongPasswordException {
        databaseLogger.info("checking login for " + username);

        boolean valid = false;
        Session session = factory.openSession();
        User user;

        // check if user exists ----------------------------------------------------------------------------------------
        user = session.get(User.class, username);
        if (user == null) throw new AccountNotFoundException();

        // check password ----------------------------------------------------------------------------------------------
        try {
            valid = user.getPassword().equals(hashPassword(password, user.getSalt()));
        } catch (NoSuchAlgorithmException e) {
            databaseLogger.severe("problem with password hashing algorithm");
            e.printStackTrace();
        } catch (InvalidKeySpecException e) {
            databaseLogger.severe("problem with password hashing key spec");
            e.printStackTrace();
        }
        session.close();

        databaseLogger.info(String.format("user %s credentials are %s", username, valid ? "valid" : "invalid"));
        if(!valid) throw new WrongPasswordException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getToken(String username) throws RemoteException, AccountNotFoundException {

        String tokenString;

        try (Session session = factory.openSession()) {
            Transaction transaction = session.beginTransaction();

            try {
                 session.get(User.class, username);
            } catch (Exception e) {
                throw new AccountNotFoundException();
            }
            Token token;
            token = session.get(Token.class, username);
            if (token == null) {
                databaseLogger.info(String.format("token for user %s does not yet exist", username));
                token = new Token(username, null, null);
            }
            Query tokenExists;
            do {
                tokenString = generateToken();
                tokenExists = session.createQuery(String.format(
                        FIND_TOKEN_QUERY,
                        tokenString
                ));
            } while (!tokenExists.getResultList().isEmpty());

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(new Date());
            calendar.add(Calendar.HOUR, 24);
            token.setTokenValid(calendar.getTime());
            token.setToken(tokenString);
            session.save(token);

            transaction.commit();

            return token.getToken();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean createAccount(String username, String password, String email)
            throws RemoteException, UsernameAlreadyExistsException, EmailAddressAlreadyInUseException {
        databaseLogger.info("creating account for " + username);

        try (Session session = factory.openSession()) {
            Transaction transaction;
            transaction = session.beginTransaction();
            if (session.get(User.class, username) != null) throw new UsernameAlreadyExistsException();
            Query emailQuery = session.createQuery(String.format(FIND_USER_EMAIL_QUERY, email));
            if (!emailQuery.getResultList().isEmpty()) throw new EmailAddressAlreadyInUseException();

            String salt = getSalt();
            User user = null;
            try {
                user = new User(username, hashPassword(password, salt), salt, email);
            } catch (NoSuchAlgorithmException e) {
                databaseLogger.severe("algorithm for hashing not recognised");
                e.printStackTrace();
            } catch (InvalidKeySpecException e) {
                databaseLogger.severe("key spec for hashing not valid");
                e.printStackTrace();
            }

            try {
                session.save(user);
                transaction.commit();
                session.close();
                return true;
            } catch (TransactionException e) {
                if (transaction != null) transaction.rollback();
                databaseLogger.severe("could not create record for user " + username);
                e.printStackTrace();
            } catch (Exception e) {
                databaseLogger.severe("problem saving update");
                e.printStackTrace();
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean remove(String token) throws RemoteException, AccountNotFoundException {
        databaseLogger.info("removing token for " + token);

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();
        try{
            Query query = session.createQuery(String.format(FIND_TOKEN_QUERY, token));
            Token tokenPersisted = (Token) query.getSingleResult();
            session.delete(tokenPersisted);
        } catch (Exception e){
            // detects when no result, nothing to remove.
        }
        transaction.commit();
        session.close();

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DatabaseGameInterface getDatabaseGameInterface() throws RemoteException {
        return databaseGameInterface;
    }
}
