package database.databaseRMI;

import gameInterfaces.dataClasses.Game;
import database.databaseUtil.Util;
import database.persistence.Card;
import database.persistence.Token;
import gameInterfaces.databaseGameInterfaces.DatabaseGameInterface;
import exceptions.CardNotFoundException;
import exceptions.DatabaseNotFoundException;
import exceptions.GameNotFoundException;
import exceptions.InvalidTokenException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.json.JSONArray;

import javax.persistence.Query;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static database.Database.databaseLogger;
import static database.databaseUtil.Util.*;
import static database.persistence.Token.FIND_TOKEN_QUERY;

/**
 * {@link DatabaseGameInterface} implementation to handle requests to the database server concerning data about games.
 * An object of this class will be bound to a port using {@link java.rmi.registry.Registry#bind(String, Remote)} and
 * will be called over RMI.
 */

public class DatabaseGameControl extends UnicastRemoteObject implements DatabaseGameInterface {

    // =================================================================================================================
    // fields ==========================================================================================================
    private SessionFactory factory;

    // =================================================================================================================
    // init ============================================================================================================

    public DatabaseGameControl() throws RemoteException, DatabaseNotFoundException {
        super();
        init();
    }

    /**
     * Method to initialise {@link #factory}.
     *
     * @throws DatabaseNotFoundException thrown when connection to the internal database fails
     */
    private void init() throws DatabaseNotFoundException {
        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Exception e){
            throw new DatabaseNotFoundException(e.getMessage());
        }
    }

    // =================================================================================================================
    // override methods ================================================================================================

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean checkToken(String token) throws RemoteException, InvalidTokenException {
        databaseLogger.info("checking login for token: " + token);
        Session session = factory.openSession();
        return Util.checkToken(token, session);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUsername(String token) throws RemoteException, InvalidTokenException {
        Session session = factory.openSession();
        Token foundToken;
        Query query = session.createQuery(String.format(FIND_TOKEN_QUERY, token));
        try{
            foundToken = (Token) query.getSingleResult();
            if (foundToken == null) throw new InvalidTokenException();
        } catch (Exception e){
            throw new InvalidTokenException();
        }
        if(!foundToken.isValid(new Date())) throw new InvalidTokenException();
        return foundToken.getUsername();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Game> getRunningGames() throws RemoteException {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] getCard(UUID cardId) throws RemoteException, CardNotFoundException {
        Session session = factory.openSession();
        Card card = (Card) session.get(Card.class, cardId.toString());
        if (card == null) databaseLogger.severe("fuck, die is weg ofwa?");
        if(card != null) return card.getImage();
        throw new CardNotFoundException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UUID uploadCard(byte[] cardImage) throws RemoteException {

        try {
            Session session = factory.openSession();
            Transaction transaction = session.beginTransaction();

            Card card = new Card(
                    generateCardId(session).toString(),
                    cardImage
            );
            session.save(card);
            transaction.commit();
            session.close();

            return UUID.fromString(card.getImageId());
        } catch (Exception e){
            e.printStackTrace();
            throw new RemoteException();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UUID persistGame(String token, ArrayList<UUID> facesIds, UUID backId, Game game) throws RemoteException, InvalidTokenException {

        if (checkToken(token)){
            try {
                Session session = factory.openSession();
                UUID gameId = generateGameId(session);
                JSONArray facesArray = generateFacesArray(game.getCards(), facesIds, game.getSolution());
                JSONArray scoresArray = generateScoresArray(game.getScores());

                database.persistence.Game persistGame = new database.persistence.Game(
                        gameId.toString(),
                        game.getName(),
                        getUsername(token),
                        game.getWidth(),
                        game.getHeight(),
                        game.getLimit(),
                        backId.toString(),
                        facesArray.toString(),
                        scoresArray.toString()
                );
                Transaction transaction = session.beginTransaction();
                session.save(persistGame);
                transaction.commit();
                session.close();
                return gameId;
            } catch (Exception e){
                e.printStackTrace();
                throw new RemoteException();
            }
        }
        else throw new InvalidTokenException();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateGame(String gameId, Game game, ArrayList<UUID> facesIds) throws RemoteException, GameNotFoundException {

        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        // find game ---------------------------------------------------------------------------------------------------
        database.persistence.Game persistedGame;
        persistedGame = session.find(database.persistence.Game.class, gameId);
        if (persistedGame == null) throw new GameNotFoundException();

        // generate arrays ---------------------------------------------------------------------------------------------
        JSONArray array = generateScoresArray(game.getScores());
        JSONArray facesArray = generateFacesArray(game.getCards(), facesIds, game.getSolution());
        persistedGame.setScores(array.toString());
        persistedGame.setCard_faces(facesArray.toString());

        // commit changes ----------------------------------------------------------------------------------------------
        session.saveOrUpdate(persistedGame);
        transaction.commit();
        session.close();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void close(UUID gameId) {
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        // find game and remove if exists ------------------------------------------------------------------------------
        // no need to remove the game if there is no data persisted to the database ------------------------------------
        database.persistence.Game game = session.find(database.persistence.Game.class, gameId.toString());
        if (game != null) {
            session.delete(game);
            // TODO delete cards ***************************************************************************************
            // TODO update global score ********************************************************************************
        }

        transaction.commit();
        session.close();
    }
}
