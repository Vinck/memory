package broker;

import brokerRMI.DatabaseServerEntryHandler;
import brokerRMI.GameEntryHandler;
import brokerManagement.ServerManager;
import brokerRMI.EntryHandler;

import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.logging.Logger;

import static brokerEntry.BrokerEntryInterface.BROKER_NAME;
import static brokerEntry.BrokerEntryInterface.BROKER_PORT;
import static databaseEntry.DatabaseEntryInterface.DATABASE_ENTRY_NAME;
import static databaseEntry.DatabaseEntryInterface.DATABASE_ENTRY_PORT;
import static gameServerEntry.GameServerEntryInterface.GAME_ENTRY_NAME;
import static gameServerEntry.GameServerEntryInterface.GAME_ENTRY_PORT;

public class Broker {

    // =================================================================================================================
    // logger ==========================================================================================================
    public static final Logger brokerLogger = Logger.getLogger("BrokerLog");

    // =================================================================================================================
    // static values ===================================================================================================
    private static int DATABASE_SERVER_INIT_COUNT = 4;
    private static int GAME_SERVER_INIT_COUNT = 2;

    // =================================================================================================================
    // main ============================================================================================================
    public static void main(String[] args){

        // load arguments ----------------------------------------------------------------------------------------------
        loadArgs(args);

        brokerLogger.info("starting broker...");

        // init ServerManager object to use in both running rmi threads ------------------------------------------------
        ServerManager serverManager = new ServerManager();

        // start database servers --------------------------------------------------------------------------------------
        for (int count = 0; count < DATABASE_SERVER_INIT_COUNT; count++) startDatabases(serverManager);

        // start game servers ------------------------------------------------------------------------------------------
        for (int count = 0; count < GAME_SERVER_INIT_COUNT; count++) startGameServers(serverManager);

        // start rmi connections ---------------------------------------------------------------------------------------
        try {
            Registry entryRegistry = LocateRegistry.createRegistry(BROKER_PORT);
            entryRegistry.rebind(BROKER_NAME, new EntryHandler(serverManager));

            Registry gameEntryRegistry = LocateRegistry.createRegistry(GAME_ENTRY_PORT);
            gameEntryRegistry.rebind(GAME_ENTRY_NAME, new GameEntryHandler(serverManager));

            Registry databaseRegistry = LocateRegistry.createRegistry(DATABASE_ENTRY_PORT);
            databaseRegistry.rebind(DATABASE_ENTRY_NAME, new DatabaseServerEntryHandler(serverManager));

            brokerLogger.info("broker started!\n");
        } catch (RemoteException e) {
            brokerLogger.severe("problem starting broker...\n");
            e.printStackTrace();
        }
    }

    private static void loadArgs(String[] args) {

        for (String arg: args) {
            if (arg.contains("--databases")) {
                try {
                    DATABASE_SERVER_INIT_COUNT = Integer.parseInt(arg.split("=")[1]);
                    brokerLogger.info(String.format(
                            "broker will start %d databases",
                            DATABASE_SERVER_INIT_COUNT
                    ));
                }
                catch (NumberFormatException e) {
                    brokerLogger.info(String.format(
                            "wrong number format for database count, using default %d",
                            DATABASE_SERVER_INIT_COUNT
                    ));
                }
            } else if (arg.contains("--application-servers")) {
                try {
                    GAME_SERVER_INIT_COUNT = Integer.parseInt(arg.split("=")[1]);
                    brokerLogger.info(String.format(
                            "broker will start %d game servers",
                            DATABASE_SERVER_INIT_COUNT
                    ));
                }
                catch (NumberFormatException e) {
                    brokerLogger.info(String.format(
                            "wrong number format for game server count, using default %d",
                            DATABASE_SERVER_INIT_COUNT
                    ));
                }
            }
        }
    }

    /**
     * Method to start a new game server. Starts the game server in a new javaVM.
     */
    private static void startGameServers(ServerManager serverManager) {
        brokerLogger.info("starting new game server....");
        serverManager.startGameServer();
        brokerLogger.info("new game server started!");
    }

    /**
     * Method to start a new database server.
     */
    private static void startDatabases(ServerManager serverManager) {
        brokerLogger.info("starting new database server....");
        serverManager.startDatabaseServer();
        brokerLogger.info("new database server started!");
    }
}