package brokerRMI;

import brokerManagement.ServerManager;
import dataClasses.Server;
import databaseEntry.DatabaseEntryInterface;

import java.rmi.RemoteException;
import java.rmi.server.RMIClientSocketFactory;
import java.rmi.server.RMIServerSocketFactory;
import java.rmi.server.UnicastRemoteObject;

public class DatabaseServerEntryHandler extends UnicastRemoteObject implements DatabaseEntryInterface {

    // =================================================================================================================
    // variables =======================================================================================================

    private final ServerManager serverManager;

    // =================================================================================================================
    // init ============================================================================================================

    public DatabaseServerEntryHandler(ServerManager serverManager) throws RemoteException {
        this.serverManager = serverManager;
    }

    // =================================================================================================================
    // interface overrides =============================================================================================

    @Override
    public void enter(String hostname, int port) throws RemoteException {
        serverManager.addDatabaseServer(new Server(hostname, port));
    }
}
