package brokerRMI;

import brokerManagement.ServerManager;
import dataClasses.Server;
import gameServerEntry.GameServerEntryInterface;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;

import static broker.Broker.brokerLogger;

public class GameEntryHandler extends UnicastRemoteObject implements GameServerEntryInterface {

    // =================================================================================================================
    // variables =======================================================================================================

    private ServerManager serverManager;

    // =================================================================================================================
    // init ============================================================================================================

    public GameEntryHandler(ServerManager serverManager) throws RemoteException {
        this.serverManager = serverManager;
    }

    // =================================================================================================================
    // interface overrides =============================================================================================

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Server> enterApplicationServerControl(String hostname, int port) throws RemoteException {
        brokerLogger.info(String.format("new game server started on %s:%d", hostname, port));
        serverManager.addGameServer(new Server(hostname, port));
        return serverManager.getGameServers();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Server enterAccountControl(String hostname, int port) throws RemoteException {
        brokerLogger.info(String.format("new game server started on %s:%d", hostname, port));
        // break if no database servers available ----------------------------------------------------------------------
        if (serverManager.getDatabaseServers().size() == 0) return null;
        serverManager.addGameServerEntryPoint(new Server(hostname, port));
        return serverManager.getNextDatabaseServer();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Server requestNewDatabase(String hostname, int port) throws RemoteException {
        return null;
    }
}
