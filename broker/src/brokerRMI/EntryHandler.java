package brokerRMI;

import brokerEntry.BrokerEntryInterface;
import brokerManagement.ServerManager;
import clientAccountInterfaces.ClientAccountInterface;
import dataClasses.Server;

import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import static clientAccountInterfaces.ClientAccountInterface.ACCOUNTS_NAME;

/**
 * Entry point for client application. Grants new client access to the network of application servers by
 * providing one of the account interfaces at the game servers.
 *
 */
public class EntryHandler extends UnicastRemoteObject implements BrokerEntryInterface {

    // =================================================================================================================
    // variables =======================================================================================================

    private ServerManager serverManager;

    // =================================================================================================================
    // init ============================================================================================================

    public EntryHandler(ServerManager serverManager) throws RemoteException {
        this.serverManager = serverManager;
    }

    // =================================================================================================================
    // method overrides ================================================================================================

    /**
     * {@inheritDoc}
     */
    @Override
    public ClientAccountInterface enter() throws RemoteException {
        // temporary variables -----------------------------------------------------------------------------------------
        ClientAccountInterface clientAccountInterface;
        Server server = null;

        // get client account interface from game server ---------------------------------------------------------------
        try {
            server = serverManager.getNextGameServerEntryPoint();
            if (server == null) return null;
            Registry registry = LocateRegistry.getRegistry(server.getHost(), server.getPort());
            clientAccountInterface = (ClientAccountInterface) registry.lookup(ACCOUNTS_NAME);
        } catch (NotBoundException e) {
            // dead server ---------------------------------------------------------------------------------------------
            serverManager.removerGameServer(server);
            return enter();
        } catch (ConnectException e) {
            // dead server ---------------------------------------------------------------------------------------------
            serverManager.removerGameServer(server);
            return enter();
        }

        return clientAccountInterface;
    }
}
