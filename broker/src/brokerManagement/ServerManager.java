package brokerManagement;

import dataClasses.Server;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import static broker.Broker.brokerLogger;

public class ServerManager {
    // =================================================================================================================
    // variables =======================================================================================================

    private LinkedList<Server> gameServers;
    private Server previousGameServer;
    private LinkedList<Server> gameServerEntryPoints;
    private Server previousGameServerEntryPoint;
    private LinkedList<Server> databaseServers;
    private Server previousDatabaseServer;

    // =================================================================================================================
    // init ============================================================================================================

    public ServerManager() {
        gameServers = new LinkedList<>();
        databaseServers = new LinkedList<>();
        gameServerEntryPoints = new LinkedList<>();

        previousGameServer = null;
        previousDatabaseServer = null;
        previousGameServerEntryPoint = null;
    }

    // =================================================================================================================
    // getters & setters ===============================================================================================

    public LinkedList<Server> getGameServers() {
        return gameServers;
    }

    public void setGameServers(LinkedList<Server> gameServers) {
        this.gameServers = gameServers;
    }

    public Server getPreviousGameServer() {
        return previousGameServer;
    }

    public void setPreviousGameServer(Server previousGameServer) {
        this.previousGameServer = previousGameServer;
    }

    public LinkedList<Server> getDatabaseServers() {
        return databaseServers;
    }

    public void setDatabaseServers(LinkedList<Server> databaseServers) {
        this.databaseServers = databaseServers;
    }

    public Server getPreviousDatabaseServer() {
        return previousDatabaseServer;
    }

    public void setPreviousDatabaseServer(Server previousDatabaseServer) {
        this.previousDatabaseServer = previousDatabaseServer;
    }


    // =================================================================================================================
    // other methods ===================================================================================================

    /**
     * Method will start a new game server in a new javaVN.
     */
    public void startGameServer() {
        try {
            Runtime.getRuntime().exec(
                    new String[]{"cmd","/c","start","cmd","/k","java -jar \"C:\\dataFolder\\programmeren\\java\\Memory\\server\\out\\server.jar\""}
            );
        } catch (IOException e) {
            brokerLogger.severe("could not start new game server...");
            e.printStackTrace();
        }
    }

    /**
     * Method will start a new database server in new javaVM.
     */
    public void startDatabaseServer() {
        try {
            Runtime.getRuntime().exec(
                    new String[]{"cmd","/c","start","cmd","/k","java -jar \"C:\\dataFolder\\programmeren\\java\\Memory\\database\\out\\database.jar\""}
            );
        } catch (IOException e) {
            brokerLogger.severe("could not start new database server...");
            e.printStackTrace();
        }
    }

    /**
     * Method to acquire information to connect to a running game server.
     *
     * @return  information to connect to running game server
     */
    public synchronized Server getNextGameServer() {
        // break if no game servers available --------------------------------------------------------------------------
        if (gameServers.size() == 0) return null;

        // else calculate next game server in list ---------------------------------------------------------------------
        previousGameServer =
                gameServers.get((gameServers.indexOf(previousGameServer) + 1) % gameServers.size());
        return previousGameServer;
    }

    /**
     * Method to acquire information to connect to running database server.
     *
     * @return  information to connect to running database server
     */
    public synchronized Server getNextDatabaseServer() {
        // break if no database servers available ----------------------------------------------------------------------
        if (databaseServers.size() == 0) return null;

        // else calculate next database server in list -----------------------------------------------------------------
        previousDatabaseServer =
                databaseServers.get((databaseServers.indexOf(previousDatabaseServer) + 1) % databaseServers.size());
        return previousDatabaseServer;
    }

    /**
     * Method for to use to get data needed to connect to a game server's account control interface
     * ({@link clientAccountInterfaces.ClientAccountInterface}).
     *
     * @return  the data needed to connect
     */
    public Server getNextGameServerEntryPoint() {
        // break if none available -------------------------------------------------------------------------------------
        if (gameServerEntryPoints.size() == 0) return null;

        // else calculate next available entry point in list -----------------------------------------------------------
        previousGameServerEntryPoint =
                gameServerEntryPoints.get(
                        (gameServerEntryPoints.indexOf(previousGameServerEntryPoint) + 1) %
                        gameServerEntryPoints.size()
                );
        return previousGameServerEntryPoint;
    }

    /**
     * Method to remove information from list of game servers to call in case a server is detected to be down.
     *
     * @param server    information to remove from the list
     */
    public synchronized void removerGameServer(Server server) {
        gameServers.remove(server);
    }

    /**
     * Method to add new server information to {@link ServerManager#gameServers}. Also sets first game server,
     * represented by {@link #previousGameServer}, if there was not one set yet.
     *
     * @param server    server info to add
     */
    public void addGameServer(Server server) {
        brokerLogger.info(String.format("adding game server at %s:%d", server.getHost(), server.getPort()));
        if (previousGameServer == null) previousGameServer = server;
        gameServers.addLast(server);
    }

    /**
     * Method to add new server information to {@link ServerManager#databaseServers}. Also sets first database server,
     * represented by {@link #previousDatabaseServer}, if there was not one set yet.
     *
     * @param server    server info to add
     */
    public void addDatabaseServer(Server server) {
        brokerLogger.info(String.format("adding database server at %s:%d", server.getHost(), server.getPort()));
        if (previousDatabaseServer == null) previousDatabaseServer = server;
        databaseServers.addLast(server);
    }

    /**
     * Method to add new server information to {@link ServerManager#gameServerEntryPoints}. Also sets firs game server
     * entry point, represented by {@link #previousGameServerEntryPoint}, if there was not one set yet.
     *
     * @param server    server info to add
     */
    public void addGameServerEntryPoint(Server server) {
        brokerLogger.info(String.format("adding game server entry point at %s:%d", server.getHost(), server.getPort()));
        if (previousGameServerEntryPoint == null) previousGameServerEntryPoint = server;
        gameServerEntryPoints.addLast(server);
    }
}
