package applicationServerControlInterfaces;

import gameInterfaces.clientGameInterfaces.ClientGameInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface ApplicationServerControlInterface extends Remote{

    // =================================================================================================================
    // static constants ================================================================================================
    String APPLICATION_CONTROL_HOST = "localhost";
    int APPLICATION_CONTROL_PORT_LOWER = 15000;
    int APPLICATION_CONTROL_PORT_UPPER = 15499;
    String APPLICATION_CONTROL_NAME = "application_control";

    // =================================================================================================================
    // methods =========================================================================================================

    /**
     * Method for other application servers to call to get the server's lobby. The lobby is returned in the form of a
     * list of {@link ClientGameInterface}s.
     *
     * @return  a list representing the server's lobby
     * @throws RemoteException  default rmi exception
     */
    List<ClientGameInterface> getRunningGames() throws RemoteException;
}
